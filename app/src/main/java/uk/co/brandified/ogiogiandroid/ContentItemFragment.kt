package uk.co.brandified.ogiogiandroid

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.TypedValue
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.cardview.widget.CardView
import androidx.core.graphics.ColorUtils
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import uk.co.brandified.ogiogiandroid.api.API
import uk.co.brandified.ogiogiandroid.api.CategoryType
import uk.co.brandified.ogiogiandroid.api.ContentItem
import uk.co.brandified.ogiogiandroid.api.DataManager
import uk.co.brandified.ogiogiandroid.databinding.FragmentCategoryItemBinding


class ContentItemFragment: Fragment() {

    private var _binding: FragmentCategoryItemBinding? = null


    private var item: ContentItem? = null
    private var type: CategoryType? = null

    private val args: ContentItemFragmentArgs by navArgs()

    private val binding get() = _binding!!

    private var menu: Menu? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setHasOptionsMenu(true)

        _binding = FragmentCategoryItemBinding.inflate(inflater, container, false)
        val root: View = binding!!.root
        // Inflate the layout for this fragment

        this.item = args.item
        this.type = args.type

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var col = Color.parseColor(type!!.color)
        var a = ColorUtils.setAlphaComponent(col, 90)

        binding.contentCard.setCardBackgroundColor(a)

        binding.wave.setBackgroundColor(a)
        binding.wave.setColorFilter(Color.WHITE)

        val language = DataManager.getLocalLanguage(requireContext())
        if(language == 0){
            binding.typeTitle.text = item!!.title_cy
        } else if(language == 1){
            binding.typeTitle.text = item!!.title_en
        }

        binding.typeTitle.setTextColor(Color.parseColor("#0F4936"))

        binding.blob.setColorFilter(col)


        API.getImage(requireContext(), item!!.icon_url, binding.typeImage)

        var found = false
        DataManager.getViewedItems(requireContext()).forEach{
            if(it.id ==  item!!.id){
                found = true
            }
        }

        if(!found){
            DataManager.addViewedItem(item!!, requireContext())
        }

        sortContent()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.action_bar_menu, menu)

        this.menu = menu

        val favourites = DataManager.getFavourites(requireContext())
        favourites.forEach{
                fav ->

            if(fav.id == this.item!!.id){
                val img = this.menu?.findItem(R.id.action_favorite)
                img?.setIcon(R.drawable.fave_on)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {

        R.id.action_favorite -> {
            // User chose the "Favorite" action, mark the current item as a favorite...

            // Retrieve the 'fave_off' drawable safely
            val faveOffDrawable = resources.getDrawable(R.drawable.fave_off, null)

            // Compare the icon's constant state to the 'fave_off' drawable's constant state safely
            if (item.icon?.constantState == faveOffDrawable?.constantState) {
                // If the current icon matches 'fave_off', set it to 'fave_on'
                item.setIcon(R.drawable.fave_on)
                DataManager.addToFavourites(this.item!!, requireContext())
            } else {
                // If the icon doesn't match 'fave_off', set it back to 'fave_off'
                item.setIcon(R.drawable.fave_off)
                DataManager.removeFromFavourites(this.item!!, requireContext())
            }
            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }


    fun sortContent(){
        item!!.blocks.forEach{
            block ->

            when(block.type) {
                "heading" -> {
                    val text = TextView(requireContext())


                    val language = DataManager.getLocalLanguage(requireContext())
                    if(language == 0){
                        text.text = block.content_cy
                    } else if(language == 1){
                        text.text = block.content_en
                    }

                    API.loadFont(requireContext(), "Roboto", "700"){
                        text.setTypeface(it)
                    }

                    text.setTextColor(Color.parseColor("#0F4936"))
                    text.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 22f)

                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )

                    params.setMargins(0,40,0,40)

                    text.layoutParams = params

                    binding!!.itemsStack.addView(text)
                }
                "text" -> {
                    val text = TextView(requireContext())

                    val language = DataManager.getLocalLanguage(requireContext())

                    if(block.content_en!!.contains("open.spotify.com") || block.content_cy!!.contains("open.spotify.com")){
                        val card = CardView(requireContext())

                        val cardParams = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            150
                        )
                        cardParams.setMargins(0,20,0,20)

                        card.radius = 80f
                        card.setCardBackgroundColor(Color.parseColor("#1D8954"))
                        card.setContentPadding(30,10,30,10)
                        card.layoutParams = cardParams
                        card.cardElevation = 0f

                        //--- stack ---
                        val stack = LinearLayout(requireContext())

                        val stackParam = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT
                        )

                        stack.layoutParams = stackParam
                        stack.orientation = LinearLayout.HORIZONTAL

                        val imageView = ImageView(requireContext())

                        val imageParams = LinearLayout.LayoutParams(
                            50,
                            LinearLayout.LayoutParams.MATCH_PARENT
                        )
                        imageParams.setMargins(10,0,0,0)

                        imageView.layoutParams = imageParams
                        imageView.setImageDrawable(resources.getDrawable(R.drawable.ic_spotify))
                        imageView.setColorFilter(Color.WHITE)


                        stack.addView(imageView)


                        val textView = TextView(requireContext())

                        println("spotify: " + block.content_en)
                        var url = ""
                        var name = ""
                        if(language == 0){
                            var split = block.content_cy!!.split("href=")
                            split = split[1].split(">")
                            url = split[0].substring(1, split[0].length - 1)
                            var nameSplit = split[1].split("</a")
                            name = nameSplit[0]
                        } else if(language == 1) {
                            var split = block.content_en!!.split("href=")
                            split = split[1].split(">")
                            url = split[0].substring(1, split[0].length - 1)
                            var nameSplit = split[1].split("</a")
                            name = nameSplit[0]
                        }

                        textView.text = name
                        textView.gravity = Gravity.CENTER

                        val textParams = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )

                        textParams.gravity = Gravity.CENTER
                        textParams.setMargins(20,0,20,0)

                        textView.layoutParams = textParams

                        API.loadFont(requireContext(), "Roboto", "700"){
                            textView.setTypeface(it)
//                        textView.setTextSize(18f)
                            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                            {
                                textView.setAutoSizeTextTypeUniformWithConfiguration(17,18,2,2)

                            } else {
                                textView.setTextSize(18f)
                            }

                            textView.setTextColor(Color.WHITE)
                        }

                        stack.addView(textView)

                        card.addView(stack)

                        //--- button ---
                        val button = Button(requireContext())

                        val buttonParams = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT
                        )

                        button.layoutParams = buttonParams
                        button.setBackgroundColor(Color.TRANSPARENT)
                        button.setOnClickListener{
                            val webIntent = Intent(Intent.ACTION_VIEW)
                            webIntent.data = Uri.parse(url)
                            startActivity(webIntent)
                        }

                        card.addView(button)

                        binding!!.itemsStack.addView(card)
                    } else {
                        var t = ""
                        if (language == 0) {
                            t = block.content_cy!!
                        } else if (language == 1) {
                            t = block.content_en!!
                        }

                        text.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            Html.fromHtml(t, Html.FROM_HTML_MODE_COMPACT)
                        } else {
                            Html.fromHtml(t)
                        }
                        text.setTextColor(Color.parseColor("#0F4936"))
                        text.setLinkTextColor(Color.parseColor("#0F4936"))
                        text.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18f)
                        text.movementMethod = LinkMovementMethod.getInstance()

                        binding!!.itemsStack.addView(text)
                    }
                }

                "image" -> {

                    val img = ImageView(requireContext())

//                    val imageParams = LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.WRAP_CONTENT,
//                        LinearLayout.LayoutParams.WRAP_CONTENT
//                    )
//                    imageParams.setMargins(0,0,0,0)
//
//                    img.layoutParams = imageParams

                    API.getImage(requireContext(), block!!.image_url, img)

                    binding!!.itemsStack.addView(img)

                }

                "address" -> {
                    val card = CardView(requireContext())

                    val cardParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        200
                    )
                    cardParams.setMargins(0,20,0,20)

                    card.radius = 50f
                    card.setCardBackgroundColor(Color.parseColor("#EFEFEF"))
                    card.setContentPadding(20,10,20,10)
                    card.layoutParams = cardParams
                    card.cardElevation = 0f
//--- stack ---
                    val stack = LinearLayout(requireContext())

                    val stackParam = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    stack.layoutParams = stackParam
                    stack.orientation = LinearLayout.HORIZONTAL

                    val imageView = ImageView(requireContext())

                    val imageParams = LinearLayout.LayoutParams(
                        80,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )
                    imageParams.setMargins(20,0,0,0)

                    imageView.layoutParams = imageParams
                    imageView.setImageDrawable(resources.getDrawable(R.drawable.ic_map_marked_alt))
                    imageView.setColorFilter(Color.parseColor("#0F4936"))


                    stack.addView(imageView)


                    val textView = TextView(requireContext())
                    textView.text = block.content_en
                    textView.gravity = Gravity.CENTER

                    val textParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )

                    textParams.gravity = Gravity.CENTER
                    textParams.setMargins(20,0,0,0)

                    textView.layoutParams = textParams

                    API.loadFont(requireContext(), "Roboto", "500"){
                        textView.setTypeface(it)
//                        textView.setTextSize(18f)
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        {
                            textView.setAutoSizeTextTypeUniformWithConfiguration(12,18,2,2)

                        } else {
                            textView.setTextSize(18f)
                        }

                        textView.setTextColor(Color.parseColor("#0F4936"))
                    }

                    stack.addView(textView)

                    card.addView(stack)

                    //--- button ---
                    val button = Button(requireContext())

                    val buttonParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    button.layoutParams = buttonParams
                    button.setBackgroundColor(Color.TRANSPARENT)
                    button.setOnClickListener({

                    })

                    card.addView(button)

                    binding!!.itemsStack.addView(card)
                }

                "date" -> {
                    val card = CardView(requireContext())

                    val cardParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        150
                    )
                    cardParams.setMargins(0,20,0,20)

                    card.radius = 80f
                    card.setCardBackgroundColor(Color.parseColor("#EFEFEF"))
                    card.setContentPadding(30,10,30,10)
                    card.layoutParams = cardParams
                    card.cardElevation = 0f
//--- stack ---
                    val stack = LinearLayout(requireContext())

                    val stackParam = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    stack.layoutParams = stackParam
                    stack.orientation = LinearLayout.HORIZONTAL

                    val imageView = ImageView(requireContext())

                    val imageParams = LinearLayout.LayoutParams(
                        80,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )
                    imageParams.setMargins(10,0,0,0)

                    imageView.layoutParams = imageParams
                    imageView.setImageDrawable(resources.getDrawable(R.drawable.ic_calendar))
                    imageView.setColorFilter(Color.parseColor("#0F4936"))


                    stack.addView(imageView)


                    val textView = TextView(requireContext())
                    textView.text = block.content_en
                    textView.gravity = Gravity.CENTER

                    val textParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )

                    textParams.gravity = Gravity.CENTER
                    textParams.setMargins(20,0,20,0)

                    textView.layoutParams = textParams

                    API.loadFont(requireContext(), "Roboto", "700"){
                        textView.setTypeface(it)
//                        textView.setTextSize(18f)
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        {
                            textView.setAutoSizeTextTypeUniformWithConfiguration(17,18,2,2)

                        } else {
                            textView.setTextSize(18f)
                        }

                        textView.setTextColor(Color.parseColor("#0F4936"))
                    }

                    stack.addView(textView)

                    card.addView(stack)

                    //--- button ---
                    val button = Button(requireContext())

                    val buttonParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    button.layoutParams = buttonParams
                    button.setBackgroundColor(Color.TRANSPARENT)
                    button.setOnClickListener({

                    })

                    card.addView(button)

                    binding!!.itemsStack.addView(card)
                }

                "email" -> {
                    val card = CardView(requireContext())

                    val cardParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        150
                    )
                    cardParams.setMargins(0,20,0,20)

                    card.radius = 80f
                    card.setCardBackgroundColor(Color.parseColor("#EFEFEF"))
                    card.setContentPadding(30,10,30,10)
                    card.layoutParams = cardParams
                    card.cardElevation = 0f
//--- stack ---
                    val stack = LinearLayout(requireContext())

                    val stackParam = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    stack.layoutParams = stackParam
                    stack.orientation = LinearLayout.HORIZONTAL

                    val imageView = ImageView(requireContext())

                    val imageParams = LinearLayout.LayoutParams(
                        80,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )
                    imageParams.setMargins(10,0,0,0)

                    imageView.layoutParams = imageParams
                    imageView.setImageDrawable(resources.getDrawable(R.drawable.ic_envelope))
                    imageView.setColorFilter(Color.parseColor("#0F4936"))


                    stack.addView(imageView)


                    val textView = TextView(requireContext())
                    textView.text = block.content_en
                    textView.gravity = Gravity.CENTER

                    val textParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )

                    textParams.gravity = Gravity.CENTER
                    textParams.setMargins(20,0,20,0)

                    textView.layoutParams = textParams

                    API.loadFont(requireContext(), "Roboto", "700"){
                        textView.setTypeface(it)
//                        textView.setTextSize(18f)
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        {
                            textView.setAutoSizeTextTypeUniformWithConfiguration(17,18,2,2)

                        } else {
                            textView.setTextSize(18f)
                        }

                        textView.setTextColor(Color.parseColor("#0F4936"))
                    }

                    stack.addView(textView)

                    card.addView(stack)

                    //--- button ---
                    val button = Button(requireContext())

                    val buttonParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    button.layoutParams = buttonParams
                    button.setBackgroundColor(Color.TRANSPARENT)
                    button.setOnClickListener({
                        try {
                            val emailIntent = Intent(Intent.ACTION_VIEW)
                            emailIntent.data = Uri.parse("mailto:")
                            emailIntent.putExtra(
                                Intent.EXTRA_EMAIL,
                                block.content_en!!.filter { !it.isWhitespace() })
                            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "")
                            startActivity(emailIntent)
                        } catch(e: ActivityNotFoundException){
                            println(e)
                        }
                    })

                    card.addView(button)

                    binding!!.itemsStack.addView(card)
                }

                "phone" -> {
                    val card = CardView(requireContext())

                    val cardParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        150
                    )
                    cardParams.setMargins(0,20,0,20)

                    card.radius = 80f
                    card.setCardBackgroundColor(Color.parseColor("#EFEFEF"))
                    card.setContentPadding(30,10,30,10)
                    card.layoutParams = cardParams
                    card.cardElevation = 0f
//--- stack ---
                    val stack = LinearLayout(requireContext())

                    val stackParam = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    stack.layoutParams = stackParam
                    stack.orientation = LinearLayout.HORIZONTAL

                    val imageView = ImageView(requireContext())

                    val imageParams = LinearLayout.LayoutParams(
                        80,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )
                    imageParams.setMargins(10,0,0,0)

                    imageView.layoutParams = imageParams
                    imageView.setImageDrawable(resources.getDrawable(R.drawable.ic_phone_alt))
                    imageView.setColorFilter(Color.parseColor("#0F4936"))


                    stack.addView(imageView)


                    val textView = TextView(requireContext())
                    textView.text = block.content_en
                    textView.gravity = Gravity.CENTER

                    val textParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )

                    textParams.gravity = Gravity.CENTER
                    textParams.setMargins(20,0,20,0)

                    textView.layoutParams = textParams

                    API.loadFont(requireContext(), "Roboto", "700"){
                        textView.setTypeface(it)
//                        textView.setTextSize(18f)
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        {
                            textView.setAutoSizeTextTypeUniformWithConfiguration(17,18,2,2)

                        } else {
                            textView.setTextSize(18f)
                        }

                        textView.setTextColor(Color.parseColor("#0F4936"))
                    }

                    stack.addView(textView)

                    card.addView(stack)

                    //--- button ---
                    val button = Button(requireContext())

                    val buttonParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    button.layoutParams = buttonParams
                    button.setBackgroundColor(Color.TRANSPARENT)
                    button.setOnClickListener({
                        val dialIntent = Intent(Intent.ACTION_DIAL)
                        dialIntent.data = Uri.parse("tel:" + block.content_en!!.filter{!it.isWhitespace()})
                        startActivity(dialIntent)
                    })

                    card.addView(button)

                    binding!!.itemsStack.addView(card)
                }

                "facebook" -> {
                    val card = CardView(requireContext())

                    val cardParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        150
                    )
                    cardParams.setMargins(0,20,0,20)

                    card.radius = 80f
                    card.setCardBackgroundColor(Color.parseColor("#4267B2"))
                    card.setContentPadding(30,10,30,10)
                    card.layoutParams = cardParams
                    card.cardElevation = 0f
//--- stack ---
                    val stack = LinearLayout(requireContext())

                    val stackParam = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    stack.layoutParams = stackParam
                    stack.orientation = LinearLayout.HORIZONTAL

                    val imageView = ImageView(requireContext())

                    val imageParams = LinearLayout.LayoutParams(
                        50,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )
                    imageParams.setMargins(10,0,0,0)

                    imageView.layoutParams = imageParams
                    imageView.setImageDrawable(resources.getDrawable(R.drawable.ic_facebook_f))
                    imageView.setColorFilter(Color.WHITE)


                    stack.addView(imageView)


                    val textView = TextView(requireContext())
                    textView.text = "Facebook"

                    /*if(block.content_en!!.contains(".com/", ignoreCase = true)){
                        val t = block.content_en!!.split(".com/")
                        textView.text = t[1]
                    } else {
                        textView.text = block.content_en
                    }*/
                    textView.gravity = Gravity.CENTER

                    val textParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )

                    textParams.gravity = Gravity.CENTER
                    textParams.setMargins(20,0,20,0)

                    textView.layoutParams = textParams

                    API.loadFont(requireContext(), "Roboto", "700"){
                        textView.setTypeface(it)
//                        textView.setTextSize(18f)
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        {
                            textView.setAutoSizeTextTypeUniformWithConfiguration(17,18,2,2)

                        } else {
                            textView.setTextSize(18f)
                        }

                        textView.setTextColor(Color.WHITE)
                    }

                    stack.addView(textView)

                    card.addView(stack)

                    //--- button ---
                    val button = Button(requireContext())

                    val buttonParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    button.layoutParams = buttonParams
                    button.setBackgroundColor(Color.TRANSPARENT)
                    button.setOnClickListener({
                        val webIntent = Intent(Intent.ACTION_VIEW)
                        webIntent.data = Uri.parse(block.content_en!!.filter{!it.isWhitespace()})
                        startActivity(webIntent)
                    })

                    card.addView(button)

                    binding!!.itemsStack.addView(card)
                }

                "instagram" -> {
                    val card = CardView(requireContext())

                    val cardParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        150
                    )
                    cardParams.setMargins(0,20,0,20)

                    card.radius = 80f
                    card.setCardBackgroundColor(Color.parseColor("#E1306C"))
                    card.setContentPadding(30,10,30,10)
                    card.layoutParams = cardParams
                    card.cardElevation = 0f
//--- stack ---
                    val stack = LinearLayout(requireContext())

                    val stackParam = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    stack.layoutParams = stackParam
                    stack.orientation = LinearLayout.HORIZONTAL

                    val imageView = ImageView(requireContext())

                    val imageParams = LinearLayout.LayoutParams(
                        50,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )
                    imageParams.setMargins(10,0,0,0)

                    imageView.layoutParams = imageParams
                    imageView.setImageDrawable(resources.getDrawable(R.drawable.ic_instagram))
                    imageView.setColorFilter(Color.WHITE)


                    stack.addView(imageView)


                    val textView = TextView(requireContext())
                    textView.text = "Instagram"

                    /*if(block.content_en!!.contains(".com/", ignoreCase = true)){
                        val t = block.content_en!!.split(".com/")
                        textView.text = t[1]
                    } else {
                        textView.text = block.content_en
                    }*/
                    textView.gravity = Gravity.CENTER

                    val textParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )

                    textParams.gravity = Gravity.CENTER
                    textParams.setMargins(20,0,20,0)

                    textView.layoutParams = textParams

                    API.loadFont(requireContext(), "Roboto", "700"){
                        textView.setTypeface(it)
//                        textView.setTextSize(18f)
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        {
                            textView.setAutoSizeTextTypeUniformWithConfiguration(17,18,2,2)

                        } else {
                            textView.setTextSize(18f)
                        }

                        textView.setTextColor(Color.WHITE)
                    }

                    stack.addView(textView)

                    card.addView(stack)

                    //--- button ---
                    val button = Button(requireContext())

                    val buttonParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    button.layoutParams = buttonParams
                    button.setBackgroundColor(Color.TRANSPARENT)
                    button.setOnClickListener({
                        val webIntent = Intent(Intent.ACTION_VIEW)
                        webIntent.data = Uri.parse(block.content_en!!.filter{!it.isWhitespace()})
                        startActivity(webIntent)
                    })

                    card.addView(button)

                    binding!!.itemsStack.addView(card)
                }

                "location" -> {
//                    val map = MapView(requireContext())
//
//                    val mapParams = LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.MATCH_PARENT,
//                        200
//                    )
//
//                    map.layoutParams = mapParams
//
//                    map.getMapAsync {
//                        val cords = block.content_en
//                        if(cords!!.contains(",")){
//                            var splitCords = cords.split(",")
//
//                            var lat = splitCords[0].toDouble()
//                            var long = splitCords[1].toDouble()
//
//                            it.addMarker(MarkerOptions().position(
//                                LatLng(
//                                    lat,
//                                    long
//                                )
//                            ))
//                        }
//                    }
//
//
//                    binding!!.itemsStack.addView(map)
                }

                "twitter" -> {
                    val card = CardView(requireContext())

                    val cardParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        150
                    )
                    cardParams.setMargins(0,20,0,20)

                    card.radius = 80f
                    card.setCardBackgroundColor(Color.parseColor("#1DA1F2"))
                    card.setContentPadding(30,10,30,10)
                    card.layoutParams = cardParams
                    card.cardElevation = 0f
//--- stack ---
                    val stack = LinearLayout(requireContext())

                    val stackParam = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    stack.layoutParams = stackParam
                    stack.orientation = LinearLayout.HORIZONTAL

                    val imageView = ImageView(requireContext())

                    val imageParams = LinearLayout.LayoutParams(
                        50,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )
                    imageParams.setMargins(10,0,0,0)

                    imageView.layoutParams = imageParams
                    imageView.setImageDrawable(resources.getDrawable(R.drawable.ic_twitter))
                    imageView.setColorFilter(Color.WHITE)


                    stack.addView(imageView)


                    val textView = TextView(requireContext())

                    if(block.content_en!!.contains(".com/", ignoreCase = true)){
                        val t = block.content_en!!.split(".com/")
                        textView.text = t[1]
                    } else {
                        textView.text = block.content_en
                    }
                    textView.gravity = Gravity.CENTER

                    val textParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )

                    textParams.gravity = Gravity.CENTER
                    textParams.setMargins(20,0,20,0)

                    textView.layoutParams = textParams

                    API.loadFont(requireContext(), "Roboto", "700"){
                        textView.setTypeface(it)
//                        textView.setTextSize(18f)
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        {
                            textView.setAutoSizeTextTypeUniformWithConfiguration(17,18,2,2)

                        } else {
                            textView.setTextSize(18f)
                        }

                        textView.setTextColor(Color.WHITE)
                    }

                    stack.addView(textView)

                    card.addView(stack)

                    //--- button ---
                    val button = Button(requireContext())

                    val buttonParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )

                    button.layoutParams = buttonParams
                    button.setBackgroundColor(Color.TRANSPARENT)
                    button.setOnClickListener({
                        val webIntent = Intent(Intent.ACTION_VIEW)
                        webIntent.data = Uri.parse(block.content_en!!.filter{!it.isWhitespace()})
                        startActivity(webIntent)
                    })

                    card.addView(button)

                    binding!!.itemsStack.addView(card)
                }

                "video" -> {
                    val video = YouTubePlayerView(requireContext())

                    val videoParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )

                    videoParams.setMargins(0,10,0,10)

                    video.layoutParams = videoParams

                   // video.getPlayerUiController().showFullscreenButton(true)
                    video.addYouTubePlayerListener(object: AbstractYouTubePlayerListener(){
                        override fun onReady(@NonNull youTubePlayer: YouTubePlayer) {
                            val videoId = block!!.content_en!!
                            youTubePlayer.cueVideo(videoId, 0f)
                        }
                    })

                    binding!!.itemsStack.addView(video)
                }

                "audio" -> {

                }
            }
        }
    }

}