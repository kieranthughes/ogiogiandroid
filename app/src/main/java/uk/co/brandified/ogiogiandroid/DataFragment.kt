package uk.co.brandified.ogiogiandroid

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import uk.co.brandified.ogiogiandroid.api.API
import uk.co.brandified.ogiogiandroid.api.DataManager
import uk.co.brandified.ogiogiandroid.databinding.FragmentDataBinding

class DataFragment : Fragment() {

    private var _binding: FragmentDataBinding? = null

    private val binding get() = _binding!!

    private var language = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentDataBinding.inflate(inflater, container, false)
        val root: View = binding!!.root

        this.language = DataManager.getLocalLanguage(requireContext())

        var lang = ""

        if(this.language == 0){
            lang = "cy"
        } else if(this.language == 1){
            lang = "en"
        }


        val localeContext = LocaleHelper.setLocale(requireActivity(), lang)
        val localeResources = localeContext.resources

        binding.dataHeader.text = localeResources.getString(R.string.data_title)
        binding.whatCollectTitle.text = localeResources.getString(R.string.data_collection_title)
        binding.whatCollectText.text = localeResources.getString(R.string.data_collection_sub)
        binding.whyCollectTitle.text = localeResources.getString(R.string.data_collection_why_title)
        binding.whyCollectText.text = localeResources.getString(R.string.data_collection_why_sub)
        binding.shareDataTitle.text = localeResources.getString(R.string.data_collection_share_title)
        binding.shareDataText.text = localeResources.getString(R.string.data_collection_share_sub)
        binding.privacyPolicyButton.text = localeResources.getString(R.string.data_privacy_policy_button)
        binding.backButton.text = localeResources.getString(R.string.data_back)


        binding.backButton.setOnClickListener {
            requireActivity().onBackPressed()
        }

        binding.privacyPolicyButton.setOnClickListener {
            val action = DataFragmentDirections.actionDataFragmentToPrivacyPolicyFragment()
            findNavController().navigate(action)
        }

        API.loadFont(requireContext(), "Roboto", "700") {
            binding.whatCollectTitle.setTypeface(it)
            binding.whyCollectTitle.setTypeface(it)
            binding.shareDataTitle.setTypeface(it)
        }

        return root
    }

}