package uk.co.brandified.ogiogiandroid.ui.dashboard

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import uk.co.brandified.ogiogiandroid.CreateFamilyFragmentDirections
import uk.co.brandified.ogiogiandroid.LocaleHelper
import uk.co.brandified.ogiogiandroid.R
import uk.co.brandified.ogiogiandroid.api.API
import uk.co.brandified.ogiogiandroid.api.DataManager
import uk.co.brandified.ogiogiandroid.databinding.FragmentProgressBinding
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*

class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel
    private var _binding: FragmentProgressBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding

    private var language = 0

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
                ViewModelProvider(this).get(DashboardViewModel::class.java)

        _binding = FragmentProgressBinding.inflate(inflater, container, false)
        val root: View = binding!!.root

        binding!!.headerImage.setColorFilter(Color.WHITE)

        binding!!.familyDetailsButton.setBackgroundColor(resources.getColor(R.color.yellowOgiOgi))

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var context : Context
        try{
            context = requireContext()

            this.language = DataManager.getLocalLanguage(context!!)

            var lang = ""

            if(this.language == 0){
                lang = "cy"
            } else if(this.language == 1){
                lang = "en"
            }


            val localeContext = LocaleHelper.setLocale(requireActivity(), lang)
            val localeResources = localeContext.resources

            binding!!.familyHeader.text = localeResources!!.getString(R.string.about_us)
            binding!!.familyDetailsButton.text = localeResources!!.getString(R.string.family_details)
            binding!!.progressOverview.text = localeResources!!.getString(R.string.overview)
            binding!!.progressSub.text = localeResources!!.getString(R.string.progress_ratings_title)
            binding!!.question1.text = localeResources!!.getString(R.string.progress_question_1)
            binding!!.question2.text = localeResources!!.getString(R.string.progress_question_2)
            binding!!.question3.text = localeResources!!.getString(R.string.progress_question_3)


            if(DataManager.getHasSkippedFamily(requireContext())){
                binding!!.contentStack.visibility = View.GONE
                binding!!.noFamilyStack.visibility = View.VISIBLE

                binding!!.createFamilyButton.setOnClickListener {
                    val action = DashboardFragmentDirections.actionNavigationProgressToEditFamilyFragment()
                    findNavController().navigate(action)
                }



            } else {
                binding!!.contentStack.visibility = View.VISIBLE
                binding!!.noFamilyStack.visibility = View.GONE

                API.getContentItemCount(context){

                    val readItems = DataManager.getViewedItems(context)
                    val readCount = readItems.size

                    val percentage = BigDecimal((readCount.toDouble()/it.toDouble() * 100)).setScale(2, RoundingMode.HALF_EVEN)


                    binding!!.readPercentLabel.text = ""+ localeResources!!.getString(R.string.read_stats_pt1) + " " + percentage + "% " + localeResources!!.getString(R.string.read_stats_pt2)
                    binding!!.progressBar.progress = percentage.toInt()

                }

                var initialFam = DataManager.getInitialFamily(context)
                var latestFam = DataManager.getLatestFamily(context)

                if(initialFam != null && latestFam != null){
                    var latestRatings = latestFam.family_ratings
                    var initialRatings = initialFam.family_ratings


                    //--- confidence ---
                    var diff1 = ((latestRatings!!.parent_welsh_confidence.toFloat() / 5f) * 100f)

                    binding!!.percentage1.text = ""+String.format("%.0f", diff1)+"%"

                    val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ")
                    println("date: "+latestFam.send_date)
                    val d = format.parse(latestFam.send_date)

                    val cal = Calendar.getInstance()
                    cal.time = d

                    binding!!.sinceLabel1.text = localeResources!!.getString(R.string.since) + " "+cal.get(Calendar.DAY_OF_MONTH)+"-"+cal.get(Calendar.MONTH)+"-"+cal.get(Calendar.YEAR)


                    //--- importance ----
                    var diff2 = ((latestRatings!!.parent_welsh_importance.toFloat() / 5f) * 100f)

                    binding!!.percentage2.text = ""+String.format("%.0f", diff2)+"%"

                    binding!!.sinceLabel2.text = localeResources!!.getString(R.string.since) + " "+cal.get(Calendar.DAY_OF_MONTH)+"-"+cal.get(Calendar.MONTH)+"-"+cal.get(Calendar.YEAR)


                    //--- share ----
                    var diff3 = ((latestRatings!!.parent_share_welsh.toFloat() / 5f) * 100f)

                    binding!!.percentage3.text = ""+String.format("%.0f", diff3)+"%"
                    binding!!.sinceLabel3.text = localeResources!!.getString(R.string.since) + " "+cal.get(Calendar.DAY_OF_MONTH)+"-"+cal.get(Calendar.MONTH)+"-"+cal.get(Calendar.YEAR)
                }



            }
        } catch (e: Exception){
            println("Error with context")
        }


        binding!!.familyDetailsButton.setOnClickListener {
            val action = DashboardFragmentDirections.actionNavigationProgressToEditFamilyFragment()
            findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    fun CalculateDifference(currentRating: Int, oldRating: Int): Pair<Int, Boolean>{
        var hasImproved = false
        var oldPercentage: Float = 0f
        var newPercentage: Float = 0f

        var diffPercentage: Float = 0f


        if(currentRating > oldRating){
            hasImproved = true
        } else {
            hasImproved = false
        }

        newPercentage = ((currentRating.toFloat() / 5f) * 100f)
        oldPercentage = ((oldRating.toFloat() / 5f) * 100f)

        diffPercentage = newPercentage - oldPercentage

        return Pair(diffPercentage.toInt(), hasImproved)
    }
}