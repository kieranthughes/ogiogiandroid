package uk.co.brandified.ogiogiandroid.ui.notifications

import android.graphics.Color
import android.opengl.Visibility
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import uk.co.brandified.ogiogiandroid.LocaleHelper
import uk.co.brandified.ogiogiandroid.R
import uk.co.brandified.ogiogiandroid.api.API
import uk.co.brandified.ogiogiandroid.api.DataManager
import uk.co.brandified.ogiogiandroid.databinding.FragmentFavouritesBinding
import uk.co.brandified.ogiogiandroid.ui.home.HomeFragmentDirections

class NotificationsFragment : Fragment() {

    private lateinit var notificationsViewModel: NotificationsViewModel
    private var _binding: FragmentFavouritesBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var language = 0

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        notificationsViewModel =
                ViewModelProvider(this).get(NotificationsViewModel::class.java)

        _binding = FragmentFavouritesBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.headerImage.setColorFilter(Color.WHITE)

        this.language = DataManager.getLocalLanguage(requireContext())

        var lang = ""

        if(this.language == 0){
            lang = "cy"
        } else if(this.language == 1){
            lang = "en"
        }


        val localeContext = LocaleHelper.setLocale(requireActivity(), lang)
        val localeResources = localeContext.resources

        binding!!.familyHeader.text = localeResources.getString(R.string.favourites)


        return root
    }

    override fun onResume() {
        super.onResume()

        getFavourites()

    }

    fun getFavourites(){

        binding.contentStack.removeAllViews()

        val favourites = DataManager.getFavourites(requireContext())

        if(favourites.isEmpty()){
            binding.contentStack.visibility = View.GONE
            binding.noLikesStack.visibility = View.VISIBLE
        } else {
            binding.contentStack.visibility = View.VISIBLE
            binding.noLikesStack.visibility = View.GONE
        }

        favourites.forEach{
            item ->
            val card = CardView(requireContext())

            val cardParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                200
            )
            cardParams.setMargins(20,20,20,20)

            card.radius = 40f
            card.setCardBackgroundColor(Color.rgb(255,255,255))
            card.setContentPadding(10,10,10,10)
            card.layoutParams = cardParams
            card.cardElevation = 10f

            //--- stack ---
            val stack = LinearLayout(requireContext())

            val stackParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            stack.layoutParams = stackParam
            stack.orientation = LinearLayout.HORIZONTAL

            //--- left card ---
            val leftCard = CardView(requireContext())

            val leftParams = LinearLayout.LayoutParams(
                200,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            /* API.GetCategoryTypeByID(requireContext(), item.type) {
                leftCard.setCardBackgroundColor(Color.parseColor(it.color))
            } */

            leftCard.radius = 20f
            leftCard.setCardBackgroundColor(Color.rgb(255,255,255))
            leftCard.setContentPadding(10,10,10,10)
            leftCard.layoutParams = leftParams
            leftCard.cardElevation = 0f

            val imageView = ImageView(requireContext())

            val imageParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            imageParams.setMargins(0,0,0,0)

            imageView.layoutParams = imageParams
            API.getImage(requireContext(), item.icon_url, imageView)

            val blobView = ImageView(requireContext())

            API.GetCategoryTypeByID(requireContext(), item.type) {
                blobView.setColorFilter(Color.parseColor(it.color))
            }

            blobView.layoutParams = imageParams
            blobView.setImageDrawable(requireContext().getDrawable(R.drawable.ic_blob))


            leftCard.addView(blobView)

            leftCard.addView(imageView)

            stack.addView(leftCard)

            //--- right card ---
            val rightCard = CardView(requireContext())

            val rightParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            rightCard.radius = 20f
            rightCard.setCardBackgroundColor(Color.rgb(255,255,255))
            rightCard.setContentPadding(20,10,10,10)
            rightCard.layoutParams = rightParams
            rightCard.elevation = 0f

            val textView = TextView(requireContext())


            if(this.language == 0){
                textView.text = item.title_cy
            } else if(this.language == 1){
                textView.text = item.title_en
            }

            textView.gravity = Gravity.CENTER

            API.loadFont(requireContext(), "Roboto", "700"){
                textView.setTypeface(it)
                textView.setTextSize(18f)
                textView.setTextColor(Color.parseColor("#0F4936"))
            }

            rightCard.addView(textView)
            stack.addView(rightCard)

            card.addView(stack)

            //--- button ---
            val button = Button(requireContext())

            val buttonParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            button.layoutParams = buttonParams
            button.setBackgroundColor(Color.TRANSPARENT)
            button.setOnClickListener({
                API.GetCategoryTypeByID(requireContext(), item.type) {
                        type ->
                    val action = NotificationsFragmentDirections.actionNavigationFavouritesToContentItemFragment(item, type)
                    findNavController().navigate(action)
                }
            })

            card.addView(button)

            binding!!.contentStack.addView(card)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}