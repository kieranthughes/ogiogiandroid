package uk.co.brandified.ogiogiandroid

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import uk.co.brandified.ogiogiandroid.api.API
import uk.co.brandified.ogiogiandroid.databinding.InitialWindowBinding


class InitialPopUp: DialogFragment() {

    private var _binding: InitialWindowBinding? = null

    private val binding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return super.onCreateView(inflater, container, savedInstanceState)

        _binding = InitialWindowBinding.inflate(inflater, container, false)
        val root: View = binding!!.root

        var view = inflater.inflate(R.layout.initial_window, null)


        binding!!.headerStack.setBackgroundColor(Color.parseColor("#89E0B1"))
        binding!!.wave.setColorFilter(Color.WHITE)
        binding!!.wave.setBackgroundColor(Color.parseColor("#89E0B1"))

        binding!!.popupTitle.text = "Anhygoel Sara!"
        binding!!.popupSubTitle.text = "'Da chi'n gwneud yn ffantastig yn mynd drwy cynnwys yr app!"

        API.loadFont(requireContext(), "Roboto", "700"){
            binding!!.popupTitle.setTypeface(it)
            binding!!.popupWhy.setTypeface(it)
            binding!!.popupSkip.setTypeface(it)
        }

        API.loadFont(requireContext(), "Roboto", "500"){
            binding!!.popupSubTitle.setTypeface(it)
            binding!!.popupSubSubTitle.setTypeface(it)
        }

        binding!!.popupSkip.setOnClickListener {
            dismiss()
        }

        binding!!.popupButton.setOnClickListener {

            val ft: FragmentTransaction = parentFragmentManager.beginTransaction()
            val fragmentToRemove: Fragment? =
                parentFragmentManager.findFragmentByTag("initial_window")
            if (fragmentToRemove != null) {
                ft.remove(fragmentToRemove)
            }
            ft.addToBackStack(null)
            ft.commit() // or ft.commitAllowingStateLoss()


            val popup = RatingPopUp()
            popup.show(parentFragmentManager, "rating_window")

        }

        return root
    }
}