package uk.co.brandified.ogiogiandroid.api

import android.R.attr.tag
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.os.Handler
import android.os.HandlerThread
import android.widget.ImageView
import android.widget.Toast
import androidx.core.provider.FontRequest
import androidx.core.provider.FontsContractCompat
import com.android.volley.*
import com.android.volley.toolbox.*
import com.beust.klaxon.JsonReader
import com.beust.klaxon.Klaxon
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.MalformedJsonException
import org.json.JSONException
import org.json.JSONObject
import uk.co.brandified.ogiogiandroid.R
import java.io.InputStream
import java.io.Serializable
import java.io.StringReader
import java.io.UnsupportedEncodingException
import java.net.URL
import java.nio.charset.Charset
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


object API {

    fun GetCategories(c: Context, callback: (ArrayList<CategoryType>) -> Unit){
        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(c)
        val url = "https://llwyfan-api.codeawesome.co.uk/api/contenttype"

// Request a string response from the provided URL.
        val stringRequest = object: StringRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->
                val resultArray = arrayListOf<CategoryType>()
                JsonReader(StringReader(response)).use {
                        reader -> reader.beginArray {
                    while (reader.hasNext()) {
                        val product = Klaxon().parse<CategoryType>(reader)
                        resultArray.add(product!!)
                    }
                }
                }

                callback(resultArray)
            },
            Response.ErrorListener {  })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Token 71c788078fb9a234dda31a6def7fabce5cfa0a68"
                return headers
            }
        }

        queue.add(stringRequest)

    }

    fun doInBackground(urls: String): Bitmap? {
        var image: Bitmap? = null
        try {
            val stream : InputStream = URL(urls).openStream()
            image = BitmapFactory.decodeStream(stream)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return image
    }

    fun getImage(c: Context, url: String, image: ImageView) {
        val glideUrl = GlideUrl(url, LazyHeaders.Builder().addHeader("Authorization", "Token 71c788078fb9a234dda31a6def7fabce5cfa0a68").build())

        Glide.with(c)
            .load(glideUrl)
            .into(image)
    }

    fun GetContentItems(c: Context, id: Int, callback: (List<ContentItem>) -> Unit){
        val queue = Volley.newRequestQueue(c)
        val url = "https://llwyfan-api.codeawesome.co.uk/api/contenttype/" + id +"/contentitem"

        val stringRequest = object: DataRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->

//                val resultArray = arrayListOf<ContentItem>()
//                JsonReader(StringReader(response)).use {
//                        reader -> reader.beginArray {
//                    while (reader.hasNext()) {
//                        val product = Klaxon().parse<ContentItem>(reader)
//                        resultArray.add(product!!)
//                    }
//                }
//                }

                val typeToken = object : TypeToken<List<ContentItem>>() {}.type
                val items = Gson().fromJson<List<ContentItem>>(response, typeToken)

                callback(items)
            },
            Response.ErrorListener {
                println("couldn't get content items")
                it.printStackTrace() })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Token 71c788078fb9a234dda31a6def7fabce5cfa0a68"
                return headers
            }
        }

        queue.add(stringRequest)
    }

    fun GetContentItemByID(c: Context, id: Int, callback: (ContentItem) -> Unit){
        val queue = Volley.newRequestQueue(c)
        val url = "https://llwyfan-api.codeawesome.co.uk/api/contentitem/" + id

        val stringRequest = object: DataRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->

                val item = Gson().fromJson(response, ContentItem::class.java)
                callback(item)
            },
            Response.ErrorListener {
                println("couldn't get content item by id: " + id)
                it.printStackTrace() })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Token 71c788078fb9a234dda31a6def7fabce5cfa0a68"
                return headers
            }
        }

        queue.add(stringRequest)
    }

    fun GetCategoryTypeByID(c: Context, id: Int, callback: (CategoryType) -> Unit){
        val queue = Volley.newRequestQueue(c)
        val url = "https://llwyfan-api.codeawesome.co.uk/api/contenttype/" + id

        val stringRequest = object: DataRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->

                println("cat response: " + response)

                val item = Gson().fromJson(response, CategoryType::class.java)
                callback(item)
            },
            Response.ErrorListener {
                println("couldn't get content item by id: " + id)
                it.printStackTrace() })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Token 71c788078fb9a234dda31a6def7fabce5cfa0a68"
                return headers
            }
        }

        queue.add(stringRequest)
    }

    fun loadFont(context: Context, font: String, weight: String, callback: (Typeface) -> Unit) {
        val query = "name=" + font + "&weight=" + weight

        val request = FontRequest(
            "com.google.android.gms.fonts",
            "com.google.android.gms",
            query,
            R.array.com_google_android_gms_fonts_certs
        )


        val handlerThread = HandlerThread("fonts").apply { start() }
        val handler = Handler(handlerThread.looper)

        FontsContractCompat.requestFont(
            context,
            request,
            object : FontsContractCompat.FontRequestCallback() {
                override fun onTypefaceRetrieved(typeface: Typeface) {
                    callback(typeface)
                }

                override fun onTypefaceRequestFailed(reason: Int) {
                    println("Couldn't get typeface")
                }
            },
            handler
        )
    }

    fun getContentItemCount(context: Context, callback: (Int) -> Unit) {
        var count = 0
        var i = 0

        GetCategories(context){
            types ->

            types.forEach{
                type ->

                GetContentItems(context, type.id){
                    items ->

                    items.forEach{
                        item ->

                        if(item.is_folder){
                            count += item.children.size
                        } else {
                            count += 1
                        }
                    }

                    if(i == types.size - 1) {
                        callback(count)
                    }

                    i++
                }
            }
        }
    }

    fun SendFamilyJSON(jsonFamily: FamilySendJson, context: Context, callback: (Boolean) -> Unit){
        val queue = Volley.newRequestQueue(context)
        val url = "https://llwyfan-api.codeawesome.co.uk/api/family_statistic/"


        val json = Gson().toJson(jsonFamily)

        val jsonBody = JSONObject(json)

        println("json: " + jsonBody)

        val request = object: JsonObjectRequest(Request.Method.POST, url, jsonBody, {
            response ->
            println(response.toString())
            callback(true)
        }, {
            println("couldn't send json")
            it.printStackTrace()
            callback(false)
        }){
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = "Token 3e5af92d0f3f39ec5a8e94a24ca4071006474542"
                headers["Content-Type"] = "application/json"
                return headers
            }
        }

        queue.add(request)
    }
}

data class CategoryType(val id: Int, val title_en: String, val title_cy: String, val created:String, val icon: String, val icon_url: String, val description_en: String, val description_cy: String, val color: String, val detail: String, val items : String, val active : Boolean  ) :Serializable

data class ContentItem(val id: Int, val type: Int, val title_en: String, val title_cy: String, val blocks : ArrayList<SubContent>, val updated: String, val order: Int, val is_folder: Boolean, val icon: String, val icon_url: String, val parent: String, val children: ArrayList<ContentItem>) : Comparable<ContentItem>, Serializable{
    override fun compareTo(o: ContentItem): Int {
        val formatter = SimpleDateFormat("E d MMM yyyy HH:mm", Locale.ENGLISH)
        val firstDate = formatter.parse(updated)
        val secondDate = formatter.parse(o.updated)


        if (firstDate < secondDate) {
            return 1
        } else {
           return -1
        }

        return 0
    }
}

class SubContent(val type: String, val content_item : Int, val content_en : String?, val content_cy: String?, val image: String, val image_url: String): Serializable


open class DataRequest(
    method: Int,
    url: String,
    listener: Response.Listener<String>,
    errorListener: Response.ErrorListener
) : StringRequest(method, url, listener,errorListener) {
    override fun parseNetworkResponse(response: NetworkResponse?): Response<String> {
        try {
            var jsonString = String(response!!.data, Charset.forName("UTF-8"))
            if(jsonString.contains("}e}")){
                println(true)
            }
            return Response.success(
                jsonString,
                HttpHeaderParser.parseCacheHeaders(response)
            )
        }  catch (e: UnsupportedEncodingException)
        {
            return Response.error(ParseError(e))
        } catch (je: JSONException)
        {
            return Response.error(ParseError(je))
        } catch(me: MalformedJsonException){
            return Response.error(ParseError(me))
        } catch(jse: JsonSyntaxException){
            return Response.error(ParseError(jse))
        }
    }
}