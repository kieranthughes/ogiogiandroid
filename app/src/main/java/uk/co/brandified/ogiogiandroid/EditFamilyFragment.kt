package uk.co.brandified.ogiogiandroid

import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.navigation.fragment.findNavController
import uk.co.brandified.ogiogiandroid.api.*
import uk.co.brandified.ogiogiandroid.databinding.FragmentEditFamilyBinding
import uk.co.brandified.ogiogiandroid.ui.home.HomeFragmentDirections
import java.text.SimpleDateFormat
import java.util.*

class EditFamilyFragment : Fragment() {
    private var _binding: FragmentEditFamilyBinding? = null

    private val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentEditFamilyBinding.inflate(inflater, container, false)
        val root: View = binding!!.root

        val language = DataManager.getLocalLanguage(requireContext())

        var lang = ""

        if(language == 0){
            lang = "cy"
        } else if(language == 1){
            lang = "en"
        }

        val localeContext = LocaleHelper.setLocale(requireActivity(), lang)
        val localeResources = localeContext.resources

        binding.familyHeader.text = localeResources.getString(R.string.family_title)
        binding.subHeader.text = localeResources.getString(R.string.family_sub)
        binding.parentTitle.text = localeResources.getString(R.string.family_parent_title)
        binding.childTitle.text = localeResources.getString(R.string.family_child_title)
        binding.addChildButton.text = localeResources.getString(R.string.family_add)
        binding.addParentButton.text = localeResources.getString(R.string.family_add)
        binding.finishButton.text = localeResources.getString(R.string.family_save)




        binding.addParentButton.setOnClickListener {
            val action = EditFamilyFragmentDirections.actionEditFamilyFragmentToAddParentFragment()
            findNavController().navigate(action)
        }

        binding.addChildButton.setOnClickListener {
            val action = EditFamilyFragmentDirections.actionEditFamilyFragmentToAddChildFragment()
            findNavController().navigate(action)
        }


        binding.finishButton.setOnClickListener {

            if(DataManager.getHasSkippedFamily(requireContext())){

                var fam = DataManager.getLatestFamily(requireContext())
                var mainParent: Parent? = null

                fam?.parents?.forEach{
                        p ->

                    if(p.main_phone){
                        mainParent = p
                    }
                }

                var postcode = fam?.post_code?.filter{!it.isWhitespace()}

                fam?.family_id = ""+mainParent!!.first_name[0]+mainParent!!.date_of_birth+postcode+System.currentTimeMillis()


                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ")
                val current = sdf.format(Date())

                fam?.send_date = current

                DataManager.saveLatestFamily(fam!!, requireContext())
                DataManager.saveInitialFamily(fam, requireContext())

                val format = SimpleDateFormat("yyyy-MM-dd")
                val date = format.format(Date())

                val sendFamily = FamilySendJson(
                    fam.family_id,
                    date,
                    fam.items_read,
                    fam.post_code,
                    mainParent!!.first_language,
                    mainParent!!.second_language,
                    fam!!.family_ratings!!.parent_share_welsh,
                    fam!!.family_ratings!!.parent_welsh_confidence,
                    fam!!.family_ratings!!.parent_welsh_importance,
                    fam.children.size
                )

                API.SendFamilyJSON(sendFamily, requireContext()){

                }


                DataManager.setHasSkippedFamily(requireContext(), false)
            }


           requireActivity().onBackPressed()
        }

        // Inflate the layout for this fragment

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    override fun onResume() {
        super.onResume()

        println("resumed")
        getParents()
        getChildren()

    }

    fun getParents(){

        binding.parentStack.removeAllViews()


        val fam = DataManager.getLatestFamily(requireContext())

        println("parents: " + fam?.parents?.size)
        fam?.parents?.forEach{
                parent ->

            val card = CardView(requireContext())

            val cardParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                250
            )
            cardParams.setMargins(0,20,0,20)

            card.radius = 40f
            card.setCardBackgroundColor(Color.parseColor("#CDE2DB"))
            card.setContentPadding(50,15,20,10)
            card.layoutParams = cardParams
            card.cardElevation = 0f

            //--- stack ---
            val stack = LinearLayout(requireContext())

            val stackParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            stack.layoutParams = stackParam
            stack.orientation = LinearLayout.HORIZONTAL


            val vStack = LinearLayout(requireContext())

            val vStackParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            vStack.layoutParams = vStackParams
            vStack.orientation = LinearLayout.VERTICAL

            val name = TextView(requireContext())
            name.text = parent.first_name
            API.loadFont(requireContext(), "Roboto", "700") {
                    face ->
                name.setTypeface(face)
                name.setTextColor(Color.parseColor("#0F4936"))
                name.setTextSize(22f)
            }
            name.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )

            val hStack = LinearLayout(requireContext())

            val hStackParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )

            hStackParams.setMargins(0,10,0,10)
            hStack.layoutParams = hStackParams
            hStack.orientation = LinearLayout.HORIZONTAL

            hStack.setPadding(0,10,0,10)

            hStack.addView(name)

            if(parent.main_phone){
                val mainPhoneImage = ImageView(requireContext())

                val imageParams = LinearLayout.LayoutParams(
                    80,
                    80
                )
                imageParams.setMargins(0,0,0,0)

                mainPhoneImage.layoutParams = imageParams
                mainPhoneImage.setImageDrawable(requireActivity().getDrawable(R.drawable.ic_ios_phone_portrait))
                mainPhoneImage.setColorFilter(Color.parseColor("#0F4936"))

                hStack.addView(mainPhoneImage)
            }

            vStack.addView(hStack)

            val language = TextView(requireContext())


            val lang = DataManager.getLocalLanguage(requireContext())

            var langCode = ""

            if(lang == 0){
                langCode = "cy"
            } else if(lang == 1){
                langCode = "en"
            }

            val localeContext = LocaleHelper.setLocale(requireActivity(), langCode)
            val localeResources = localeContext.resources
            language.text = localeResources.getString(R.string.family_change)
            API.loadFont(requireContext(), "Roboto", "700") {
                    face ->
                language.setTypeface(face)
                language.setTextColor(Color.parseColor("#0F4936"))
                language.alpha = 0.75f
                language.setTextSize(16f)
            }

            vStack.addView(language)

            stack.addView(vStack)


            card.addView(stack)

            //--- button ---
            val button = Button(requireContext())

            val buttonParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            button.layoutParams = buttonParams
            button.setBackgroundColor(Color.TRANSPARENT)
            button.setOnClickListener{
                val action = EditFamilyFragmentDirections.actionEditFamilyFragmentToEditParentFragment2()
                action.parent = parent
                findNavController().navigate(action)
            }

            card.addView(button)

            binding!!.parentStack.addView(card)

        }

    }

    fun getChildren(){
        binding.childStack.removeAllViews()


        val fam = DataManager.getLatestFamily(requireContext())

        fam?.children?.forEach{
                child ->

            val card = CardView(requireContext())

            val cardParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                250
            )
            cardParams.setMargins(0,20,0,20)

            card.radius = 40f
            card.setCardBackgroundColor(Color.parseColor("#CDE2DB"))
            card.setContentPadding(50,15,20,10)
            card.layoutParams = cardParams
            card.cardElevation = 0f

            //--- stack ---
            val stack = LinearLayout(requireContext())

            val stackParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            stack.layoutParams = stackParam
            stack.orientation = LinearLayout.HORIZONTAL


            val vStack = LinearLayout(requireContext())

            val vStackParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            vStackParams.setMargins(0,10,0,10)
            vStack.setPadding(0,10,0,10)
            vStack.layoutParams = vStackParams
            vStack.orientation = LinearLayout.VERTICAL

            val name = TextView(requireContext())
            name.text = child.first_name
            API.loadFont(requireContext(), "Roboto", "700") {
                    face ->
                name.setTypeface(face)
                name.setTextColor(Color.parseColor("#0F4936"))
                name.setTextSize(22f)
            }

            name.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )

            vStack.addView(name)

            val language = TextView(requireContext())


            val lang = DataManager.getLocalLanguage(requireContext())

            var langCode = ""

            if(lang == 0){
                langCode = "cy"
            } else if(lang == 1){
                langCode = "en"
            }

            val localeContext = LocaleHelper.setLocale(requireActivity(), langCode)
            val localeResources = localeContext.resources
            language.text = localeResources.getString(R.string.family_change)
            API.loadFont(requireContext(), "Roboto", "700") {
                    face ->
                language.setTypeface(face)
                language.setTextColor(Color.parseColor("#0F4936"))
                language.alpha = 0.75f
                language.setTextSize(16f)
            }

            val subParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            subParams.setMargins(0,10,0,10)
            language.layoutParams = subParams

            vStack.addView(language)

            stack.addView(vStack)


            card.addView(stack)

            //--- button ---
            val button = Button(requireContext())

            val buttonParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            button.layoutParams = buttonParams
            button.setBackgroundColor(Color.TRANSPARENT)
            button.setOnClickListener{
                val action = EditFamilyFragmentDirections.actionEditFamilyFragmentToEditChildFragment2()
                action.child = child
                findNavController().navigate(action)
            }

            card.addView(button)

            binding!!.childStack.addView(card)

        }
    }

}