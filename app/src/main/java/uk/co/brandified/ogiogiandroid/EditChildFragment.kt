package uk.co.brandified.ogiogiandroid

import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import uk.co.brandified.ogiogiandroid.api.Child
import uk.co.brandified.ogiogiandroid.api.DataManager
import uk.co.brandified.ogiogiandroid.api.Languages
import uk.co.brandified.ogiogiandroid.api.Parent
import uk.co.brandified.ogiogiandroid.databinding.FragmentEditChildBinding
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class EditChildFragment : Fragment(), AdapterView.OnItemSelectedListener {

    private var _binding: FragmentEditChildBinding? = null

    private val binding get() = _binding!!

    private var firstLanguage: String = ""
    private var secondLanguage: String = ""

    private var language = 0

    private var child: Child? = null

    private val args: EditChildFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEditChildBinding.inflate(inflater, container, false)
        val root: View = binding!!.root


        child = args.child

        val language = DataManager.getLocalLanguage(requireContext())


        var lang = ""

        if(language == 0){
            lang = "cy"
        } else if(language == 1){
            lang = "en"
        }

        val localeContext = LocaleHelper.setLocale(requireActivity(), lang)
        val localeResources = localeContext.resources

        binding.parentTitle.text = localeResources.getString(R.string.edit_child_title)
        var childText = localeResources.getString(R.string.add_parent_subtitle)
        var content = SpannableString(localeResources.getString(R.string.add_parent_subtitle))

        fun ignoreCaseOpt(ignoreCase: Boolean) =
            if (ignoreCase) setOf(RegexOption.IGNORE_CASE) else emptySet()

        fun String?.indexesOf(pat: String, ignoreCase: Boolean = true): List<Int> =
            pat.toRegex(ignoreCaseOpt(ignoreCase))
                .findAll(this?: "")
                .map { it.range.first }
                .toList()

        var sIndex = 0
        if(language == 0){
            sIndex = childText.indexesOf("Darllen mwy...", true).first()
        } else if(language == 1){
            sIndex = childText.indexesOf("Read more...", true).first()
        }

        content.setSpan(UnderlineSpan(), sIndex, content.length, 0)
        binding.childTitle.text = content
        binding.childTitle.setOnClickListener {
            val action = EditChildFragmentDirections.actionEditChildFragment2ToDataFragment()
            findNavController().navigate(action)
        }


        binding.firstNameTitle.text = localeResources.getString(R.string.add_first_name)
        binding.AgeTitle.text = localeResources.getString(R.string.add_age)
        binding.FirstLanguageTitle.text = localeResources.getString(R.string.add_first_language)
        binding.SecondLanguageTitle.text = localeResources.getString(R.string.add_second_language)
        binding.finishButton.text = localeResources.getString(R.string.family_save)
        binding.ageWrongFormatEdit.text = localeResources.getString(R.string.parent_age_error)






        binding.finishButton.setOnClickListener {

            var age = 0

            if (Build.VERSION.SDK_INT > 26) {
                try {
                    //--- this seems dodgy ---
                    val sdf = SimpleDateFormat("yyyy-MM-dd")

                    val d = sdf.parse(binding.ageBox.text.toString())
                    val currentDate = Date()

                    val a = Calendar.getInstance(Locale.ENGLISH)
                    a.time = d

                    val b = Calendar.getInstance(Locale.ENGLISH)
                    b.time = currentDate

                    var diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR)
                    if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
                        (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(
                            Calendar.DATE
                        ))) {
                        diff--;
                    }

                    age = diff

                    var c = Child(
                        first_name = binding.firstNameBox.text.toString(),
                        age,
                        firstLanguage,
                        secondLanguage,
                        3,
                        binding.ageBox.text.toString()
                    )

                    val fam = DataManager.getLatestFamily(requireContext())
                    fam?.children!!.forEach{
                        if(it.first_name == child!!.first_name){
                            it.first_name = c.first_name
                            it.age = c.age
                            it.first_language = c.first_language
                            it.second_language = c.second_language
                            it.date_of_birth = c.date_of_birth
                        }
                    }

                    DataManager.saveLatestFamily(fam!!, requireContext())

                    requireActivity().onBackPressed()

                } catch(pe: ParseException) {
                    binding.ageWrongFormatEdit.visibility = View.VISIBLE
                    binding.mainScroll.scrollTo(0,binding.ageBox.top + 20)
                }
            }

        }

        val spinnerArray = ArrayList<String>()
        val sorted = Languages.map.toSortedMap()


        sorted.forEach{
                (key, value) ->

            spinnerArray.add(key)
        }

        var adapter = ArrayAdapter<String>(
            requireContext(), android.R.layout.simple_spinner_item, spinnerArray
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.firstLanguageSpinner.adapter = adapter
        binding.secondLanguageSpinner.adapter = adapter

//        ArrayAdapter.createFromResource(
//            requireContext(), R.array.language_array,
//            android.R.layout.simple_spinner_item
//        ).also {
//                adapter ->
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//
//            binding.firstLanguageSpinner.adapter = adapter
//            binding.secondLanguageSpinner.adapter = adapter
//        }

        binding.firstLanguageSpinner.onItemSelectedListener = this
        binding.secondLanguageSpinner.onItemSelectedListener = this

        if(child != null){
            binding.firstNameBox.setText(child!!.first_name)
            binding.ageBox.setText(child!!.date_of_birth)

            var i = 0
            sorted.forEach{
                    (key, value) ->

                if(value == child!!.first_language){
                    binding.firstLanguageSpinner.setSelection(i)
                }

                if(value == child!!.second_language){
                    binding.secondLanguageSpinner.setSelection(i)
                }
                i++
            }
        }

        return root

    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        // An item was selected. You can retrieve the selected item using

        var map = Languages.map.toSortedMap()

        if(parent.id == binding.firstLanguageSpinner.id){

            var i = 0
            map.forEach{
                    (key, value) ->

                if(i == pos){
                    firstLanguage = value
                }

                i++
            }

            //firstLanguage = pos + 1
        }
        if(parent.id == binding.secondLanguageSpinner.id){
            //secondLanguage = pos + 1
            var i = 0
            map.forEach{
                    (key, value) ->

                if(i == pos){
                    secondLanguage = value
                }

                i++
            }
        }

    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Another interface callback
    }

}