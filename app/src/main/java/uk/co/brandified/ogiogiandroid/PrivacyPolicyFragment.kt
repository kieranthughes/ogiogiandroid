package uk.co.brandified.ogiogiandroid

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import uk.co.brandified.ogiogiandroid.api.DataManager
import uk.co.brandified.ogiogiandroid.databinding.FragmentPrivacyPolicyBinding

class PrivacyPolicyFragment : Fragment() {
    private var _binding: FragmentPrivacyPolicyBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPrivacyPolicyBinding.inflate(inflater, container, false)
        val root: View = binding!!.root

        binding.webView.settings.javaScriptEnabled = true

        val language = DataManager.getLocalLanguage(requireContext())

        if(language == 0){
            binding.webView.loadUrl("file:///android_asset/privacy_cy.html")
        } else if(language == 1){
            binding.webView.loadUrl("file:///android_asset/privacy_en.html")
        }

        var lang = ""

        if(language == 0){
            lang = "cy"
        } else if(language == 1){
            lang = "en"
        }

        val localeContext = LocaleHelper.setLocale(requireActivity(), lang)
        val localeResources = localeContext.resources

        binding.button.text = localeResources.getString(R.string.data_back)

        binding.button.setOnClickListener {
            requireActivity().onBackPressed()
        }

        return root
    }
}