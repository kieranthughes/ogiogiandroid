package uk.co.brandified.ogiogiandroid.api

import android.content.Context
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Serializable
import java.lang.reflect.Type
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class Family(
    var family_id : String = "",
    var parents: ArrayList<Parent> = ArrayList<Parent>(),
    var children: ArrayList<Child> = ArrayList<Child>(),
    var items_read: Int = 0,
    var stories_read: Int = 0,
    var songs_listened: Int = 0,
    var send_date : String = "",
    var family_ratings: Ratings?,
    var items_read_objects : ArrayList<Int> = ArrayList<Int>(),
    var post_code : String = ""
): Serializable

class Parent(
    var first_name : String = "",
    var age : Int = 0,
    var first_language : String = "",
    var second_language : String = "",
    var main_phone : Boolean = false,
    var role : Int = 0,
    var welsh_rating: Int = 0,
    var date_of_birth : String = ""
): Serializable

class Child(
    var first_name : String = "",
    var age : Int = 0,
    var first_language : String = "",
    var second_language : String = "",
    var role : Int = 0,
    var date_of_birth : String = ""
): Serializable

class Ratings(
    var parent_welsh_confidence : Int = 0,
    var parent_welsh_importance : Int = 0,
    var parent_share_welsh: Int = 0
)

class FamilySendJson(
    var family_id: String = "",
    var submitted: String = "",
    var items_read: Int = 0,
    var postcode: String = "",
    var first_language: String = "",
    var second_language: String = "",
    var parent_welsh_share: Int = 0,
    var parent_welsh_confidence: Int = 0,
    var parent_welsh_importance: Int = 0,
    var children: Int = 0
)

object Languages {
    public var map: HashMap<String, String> = hashMapOf(
        "Cymraeg" to "cy",
        "English" to "en",
        "Afrikaans" to "af",
        "Shqip" to "sq",
        "Deutsch" to "de",
        "አማርኛ" to "am",
        "العربية" to "ar",
        "Հայերեն" to "hy",
        "euskara" to "eu",
        "беларуская мова" to "be",
        "বাংলা" to "bn",
        "bosanski jezik" to "bs",
        "български език" to "bg",
        "ဗမာစာ" to "my",
        "कॉशुर, کٲشُر" to "ks",
        "català" to "ca",
        "Kernewek" to "kw",
        "한국어" to "ko",
        "hrvatski jezik" to "hr",
        "Kurdî" to "ku",
        "dansk" to "da",
        "Italiano" to "it",
        "eesti" to "et",
        "Tshivenḓa" to "ve",
        "føroyskt" to "fo",
        "suomi" to "fi",
        "français" to "fr",
        "Fulfulde" to "ff",
        "Tiếng Việt" to "vi",
        "Gàidhlig" to "gd",
        "Galego" to "gl",
        "Ελληνικά" to "el",
        "ગુજરાતી" to "gu",
        "Gaeilge" to "ga",
        "עברית" to "he",
        "हिन्दी" to "hi",
        "magyar" to "hu",
        "ייִדיש" to "ji",
        "Asụsụ Igbo" to "ig",
        "Bahasa Indonesia" to "id",
        "Yorùbá" to "yo",
        "Nederlands" to "nl",
        "Íslenska" to "is",
        "日本語" to "ja",
        "latviešu valoda" to "lv",
        "lietuvių kalba" to "lt",
        "brezhoneg" to "br",
        "македонски јазик" to "mk",
        "മലയാളം" to "ml",
        "Bahasa Melayu" to "ms",
        "Malti" to "mt",
        "Gaelg" to "gv",
        "te reo Māori" to "mi",
        "नेपाली" to "ne",
        "Norsk" to "no",
        "پښتو" to "ps",
        "Português" to "pt",
        "ਪੰਜਾਬੀ" to "pa",
        "język polski" to "pl",
        "Rumantsch Grischun" to "rm",
        "Română" to "ro",
        "русский" to "ru",
        "Español" to "es",
        "српски језик" to "sr",
        "slovenčina" to "sk",
        "Slovenski jezik" to "sl",
        "Soomaaliga" to "so",
        "Kiswahili" to "sw",
        "Svenska" to "sv",
        "isiZulu" to "zu",
        "Wikang Tagalog" to "tl",
        "ไทย" to "th",
        "čeština" to "cs",
        "廣東話" to "zh-hk",
        "官话" to "zh-cn",
        "Xitsonga" to "ts",
        "Setswana" to "tn",
        "Türkçe" to "tr",
        "Walon" to "wa",
        "Українська" to "ua",
        "ئۇيغۇرچ," to "ug",
        "اردو" to "ur",
        "isiXhosa" to "xh"
    )
}

object DataManager {


    fun saveLatestFamily(fam: Family, context: Context) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()
        val gson = Gson()
        val json = gson.toJson(fam)
        editor.putString("latest_family", json) ///"TAG_LIST" is a key must same for getting data
        editor.apply()
    }

    fun saveInitialFamily(fam: Family, context: Context) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()
        val gson = Gson()
        val json = gson.toJson(fam)
        editor.putString("initial_family", json) ///"TAG_LIST" is a key must same for getting data
        editor.apply()
    }

    fun getLatestFamily(context: Context): Family? {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = prefs.getString("latest_family", null)
        val listType: Type = object : TypeToken<Family?>() {}.type
        //val array = gson.fromJson<Any>(json, listType)
        return gson.fromJson(json, Family::class.java)
    }

    fun getInitialFamily(context: Context): Family? {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = prefs.getString("initial_family", null)
        val listType: Type = object : TypeToken<Family?>() {}.type
        //val array = gson.fromJson<Any>(json, listType)
        return gson.fromJson(json, Family::class.java)
    }

    fun getHasCreatedFamily(context: Context): Boolean{
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val bool = prefs.getBoolean("has_created_family", false)
        return bool
    }

    fun setHasCreatedFamily(bool: Boolean, context: Context){
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()
        editor.putBoolean("has_created_family", bool) ///"TAG_LIST" is a key must same for getting data
        editor.apply()
    }

    fun getFavourites(context: Context): ArrayList<ContentItem>{
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = prefs.getString("favourites", null)
        if(json != null){
            val listType: Type = object : TypeToken<ArrayList<ContentItem>>() {}.type
            //val array = gson.fromJson<Any>(json, listType)
            return gson.fromJson(json, listType)
        }
        return ArrayList<ContentItem>()
    }

    fun addToFavourites(item: ContentItem, context: Context){
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()
        val favourites = getFavourites(context)
        favourites.add(item)

        val gson = Gson()
        val json = gson.toJson(favourites)
        editor.putString("favourites", json) ///"TAG_LIST" is a key must same for getting data
        editor.apply()
    }

    fun removeFromFavourites(item: ContentItem, context: Context) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()
        val favourites = getFavourites(context)

        var i = 0
        favourites.forEach{
            fav ->

            if(fav.id == item.id){
                favourites.removeAt(i)
            }
            i++
        }

        val gson = Gson()
        val json = gson.toJson(favourites)
        editor.putString("favourites", json) ///"TAG_LIST" is a key must same for getting data
        editor.apply()
    }

    fun getViewedItems(context: Context): ArrayList<ContentItem>{
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = prefs.getString("viewed_items", null)
        if(json != null){
            val listType: Type = object : TypeToken<ArrayList<ContentItem>>() {}.type
            //val array = gson.fromJson<Any>(json, listType)
            return gson.fromJson(json, listType)
        }
        return ArrayList<ContentItem>()
    }

    fun addViewedItem(item: ContentItem, context:Context){
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()
        val viewed = getViewedItems(context)
        viewed.add(item)

        val gson = Gson()
        val json = gson.toJson(viewed)
        editor.putString("viewed_items", json) ///"TAG_LIST" is a key must same for getting data
        editor.apply()
    }

    fun clearViewedItems(context: Context){
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()
        val viewed = getViewedItems(context)
        viewed.clear()

        val gson = Gson()
        val json = gson.toJson(viewed)
        editor.putString("viewed_items", json) ///"TAG_LIST" is a key must same for getting data
        editor.apply()
    }

    fun shouldShowRatingsPopUp(context: Context): Boolean{
        val date = Calendar.getInstance()
        val day = date.get(Calendar.DAY_OF_MONTH)

        var bool = false

        if(day == 1){
            bool = true
        }

        return bool
    }

    fun setHasShownRatingsPopUp(context: Context, bool: Boolean){
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()

        editor.putBoolean("has_shown_ratings", bool) ///"TAG_LIST" is a key must same for getting data
        editor.apply()
    }

    fun getHasShownRatingsPopUp(context: Context): Boolean{
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val bool = prefs.getBoolean("has_shown_ratings", false)

        return bool
    }

    fun setLocalLanguage(context: Context, lang: Int){
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()

        editor.putInt("local_language", lang) ///"TAG_LIST" is a key must same for getting data
        editor.apply()
    }

    fun getLocalLanguage(context: Context): Int {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val lang = prefs.getInt("local_language", 0)

        return lang
    }

    fun setHasSkippedFamily(context: Context, skipped: Boolean){
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()

        editor.putBoolean("skipped_family", skipped) ///"TAG_LIST" is a key must same for getting data
        editor.apply()
    }

    fun getHasSkippedFamily(context: Context): Boolean {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val skipped = prefs.getBoolean("skipped_family", false)

        return skipped
    }

}
