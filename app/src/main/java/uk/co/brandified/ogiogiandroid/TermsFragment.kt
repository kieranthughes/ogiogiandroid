package uk.co.brandified.ogiogiandroid

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import uk.co.brandified.ogiogiandroid.api.DataManager
import uk.co.brandified.ogiogiandroid.databinding.FragmentTermsBinding


class TermsFragment : Fragment() {
    private var _binding: FragmentTermsBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentTermsBinding.inflate(inflater, container, false)
        val root: View = binding!!.root


        binding.webView.settings.javaScriptEnabled = true


        val language = DataManager.getLocalLanguage(requireContext())

        if(language == 0){
            binding.webView.loadUrl("file:///android_asset/terms_cy.html")
        } else if(language == 1){
            binding.webView.loadUrl("file:///android_asset/terms_en.html")
        }


        var lang = ""

        if(language == 0){
            lang = "cy"
        } else if(language == 1){
            lang = "en"
        }

        val localeContext = LocaleHelper.setLocale(requireActivity(), lang)
        val localeResources = localeContext.resources

        binding.button.text = localeResources.getString(R.string.terms_accept)
        binding.button.setBackgroundResource(R.drawable.button_radius)
        binding.button.setTextColor(Color.parseColor("#111111"))

        binding.button.setOnClickListener {
            val action = TermsFragmentDirections.actionTermsFragmentToCreateFamilyFragment()
            findNavController().navigate(action)
        }
        // Inflate the layout for this fragment

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

}