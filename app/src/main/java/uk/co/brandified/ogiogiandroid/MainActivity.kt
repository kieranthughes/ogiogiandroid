package uk.co.brandified.ogiogiandroid

import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import uk.co.brandified.ogiogiandroid.databinding.ActivityMainBinding
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.core.content.ContextCompat
import android.view.WindowManager

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        val navHostFragment =  supportFragmentManager
            .findFragmentById(R.id.fragmentContainerView) as NavHostFragment

        val navController = navHostFragment.navController

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        //val navController = findNavController(R.id.nav_host_fragment_activity_main)

        //supportActionBar?.hide()

//        if (Build.VERSION.SDK_INT > 9) {
//            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
//            StrictMode.setThreadPolicy(policy)
//        }

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(setOf(
                R.id.navigation_home, R.id.navigation_progress, R.id.navigation_favourites, R.id.navigation_settings))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        supportActionBar?.apply {
            //title = "OgiOgi"
            setDisplayShowHomeEnabled(true)
            setDisplayUseLogoEnabled(true)
            setLogo(R.drawable.ogi_nav)
            setBackgroundDrawable(ColorDrawable(Color.parseColor("#063B29"))) // Replace with your desired color code
        }

        navController.addOnDestinationChangedListener { _, destination, _ ->
            if(destination.id == R.id.languageFragment)
            {
                binding.navView.visibility = View.GONE
                //supportActionBar?.hide()
                //supportActionBar?.setDisplayShowHomeEnabled(false)
                supportActionBar?.setDisplayHomeAsUpEnabled(false)
            } else if(destination.id == R.id.termsFragment
                || destination.id == R.id.createFamilyFragment
                || destination.id == R.id.addChildFragment
                || destination.id == R.id.addParentFragment
                || destination.id == R.id.editFamilyFragment
                || destination.id == R.id.dataFragment
                || destination.id == R.id.privacyPolicyFragment
                || destination.id == R.id.editParentFragment2
                || destination.id == R.id.editChildFragment2
            ){

                binding.navView.visibility = View.GONE
                supportActionBar?.show()
            } else if(destination.id == R.id.categoryTypeFragment || destination.id == R.id.contentItemFragment) {
                binding.navView.visibility = View.VISIBLE
                supportActionBar?.show()
            } else {
                binding.navView.visibility = View.VISIBLE
                //supportActionBar?.setDisplayShowHomeEnabled(false)
            }
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.fragmentContainerView)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }


}