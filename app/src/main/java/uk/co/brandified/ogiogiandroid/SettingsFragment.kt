package uk.co.brandified.ogiogiandroid
import uk.co.brandified.ogiogiandroid.BuildConfig
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import uk.co.brandified.ogiogiandroid.api.DataManager
import uk.co.brandified.ogiogiandroid.databinding.FragmentSettingsBinding
import java.util.*

class SettingsFragment : Fragment(), AdapterView.OnItemSelectedListener {

    private var _binding: FragmentSettingsBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private var language = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentSettingsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        this.language = DataManager.getLocalLanguage(requireContext())

        val lang = if (this.language == 0) "cy" else "en"

        val localeContext = LocaleHelper.setLocale(requireActivity(), lang)
        val localeResources = localeContext.resources

        binding.familyHeader.text = localeResources.getString(R.string.settings)
        binding.appLanguageHeader.text = localeResources.getString(R.string.app_language)
        binding.aboutAppHeader.text = localeResources.getString(R.string.about_app_title)
        binding.aboutApp.text = localeResources.getString(R.string.about_app)
        binding.versionHeader.text = localeResources.getString(R.string.version_title)
        binding.deleteDataButton.text = localeResources.getString(R.string.delete_details)

        // Use string templates for better readability
        binding.versionLabel.text = "Version: ${BuildConfig.VERSION_NAME} Build: ${BuildConfig.VERSION_CODE}"

        binding.headerImage.setColorFilter(Color.WHITE)

        binding.deleteDataButton.setBackgroundColor(Color.parseColor("#EDA4BA"))
        binding.deleteDataButton.setOnClickListener {
            val popup = DeleteDataPopUp()
            popup.show(childFragmentManager, "delete_data_window")
        }

        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.language_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.languageSpinner.adapter = adapter
        }

        binding.languageSpinner.setSelection(this.language)
        binding.languageSpinner.onItemSelectedListener = this

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        val lang = if (pos == 0) "cy" else "en"
        LocaleHelper.setLocale(requireActivity(), lang)
        DataManager.setLocalLanguage(requireContext(), pos)
        refreshView()
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Interface callback
    }

    private fun refreshView() {
        this.language = DataManager.getLocalLanguage(requireContext())

        val lang = if (this.language == 0) "cy" else "en"

        val localeContext = LocaleHelper.setLocale(requireActivity(), lang)
        val localeResources = localeContext.resources

        binding.familyHeader.text = localeResources.getString(R.string.settings)
        binding.appLanguageHeader.text = localeResources.getString(R.string.app_language)
        binding.aboutAppHeader.text = localeResources.getString(R.string.about_app_title)
        binding.aboutApp.text = localeResources.getString(R.string.about_app)
        binding.versionHeader.text = localeResources.getString(R.string.version_title)
        binding.deleteDataButton.text = localeResources.getString(R.string.delete_details)
    }
}