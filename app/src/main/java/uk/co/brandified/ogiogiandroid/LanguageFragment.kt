package uk.co.brandified.ogiogiandroid

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import uk.co.brandified.ogiogiandroid.api.API
import uk.co.brandified.ogiogiandroid.api.DataManager
import uk.co.brandified.ogiogiandroid.databinding.FragmentCategoryTypeBinding
import uk.co.brandified.ogiogiandroid.databinding.FragmentLanguageBinding
import uk.co.brandified.ogiogiandroid.ui.home.HomeFragmentDirections

class LanguageFragment : Fragment(R.layout.fragment_language) {

    private var _binding: FragmentLanguageBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentLanguageBinding.inflate(inflater, container, false)
        val root: View = binding!!.root



        binding.welshButton.setOnClickListener {
            DataManager.setLocalLanguage(requireContext(), 0)
            val action = LanguageFragmentDirections.actionLanguageFragmentToTermsFragment()
            findNavController().navigate(action)
        }

        binding.englishButton.setOnClickListener {
            DataManager.setLocalLanguage(requireContext(), 1)
            val action = LanguageFragmentDirections.actionLanguageFragmentToTermsFragment()
            findNavController().navigate(action)
        }
        // Inflate the layout for this fragment


        if(DataManager.getHasCreatedFamily(requireContext()) || DataManager.getHasSkippedFamily(requireContext())){
            val action = LanguageFragmentDirections.actionLanguageFragmentToNavigationHome()
            findNavController().navigate(action)
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

}