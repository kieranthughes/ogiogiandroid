package uk.co.brandified.ogiogiandroid.ui.home

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.children
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.*
import uk.co.brandified.ogiogiandroid.InitialPopUp
import uk.co.brandified.ogiogiandroid.LocaleHelper
import uk.co.brandified.ogiogiandroid.R
import uk.co.brandified.ogiogiandroid.databinding.FragmentHomeBinding
import uk.co.brandified.ogiogiandroid.api.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    private var categories : ArrayList<CategoryType> = ArrayList<CategoryType>()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding

    private var mainContext: Context? = null

    private var mainInflater: LayoutInflater? = null

    private var language: Int = 0


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
                ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding!!.root

        this.mainContext = requireContext()
        this.mainInflater = inflater


        this.language = DataManager.getLocalLanguage(mainContext!!)



        val retrievedFam = DataManager.getLatestFamily(requireContext())

        var lang = ""

        if(this.language == 0){
            lang = "cy"
        } else if(this.language == 1){
            lang = "en"
        }

        val localeContext = LocaleHelper.setLocale(requireActivity(), lang)
        val localeResources = localeContext.resources

        //--- locale ---
        getHeader()

        binding!!.contentTitle.text = localeResources!!.getString(R.string.home_title)
        API.loadFont(requireContext(), "Roboto", "700"){
            binding!!.contentTitle.setTypeface(it)
            binding!!.contentTitle.setTextColor(Color.parseColor("#0F4936"))
            binding!!.latestContentTitle.setTypeface(it)
            binding!!.latestContentTitle.setTextColor(Color.parseColor("#0F4936"))
        }

        API.loadFont(requireContext(), "Roboto", "400"){
            binding!!.latestContentSubTitle.setTypeface(it)
            binding!!.latestContentSubTitle.setTextColor(Color.parseColor("#0F4936"))
        }

        binding!!.latestContentTitle.text = localeResources!!.getString(R.string.home_latest_title)
        binding!!.latestContentSubTitle.text = localeResources!!.getString(R.string.home_latest_sub)






        val contentStack: LinearLayout = binding!!.contentStack


        runBlocking {
            val job = GlobalScope.launch(Dispatchers.Default) {
                binding!!.contentLoadingBar.isVisible = true
                var numOfCategories = 0
                API.GetCategories(requireContext()) {
                    categories = it
                    it.forEach {
                        if(numOfCategories % 1 == 0){
                            val stack = LinearLayout(requireContext())

                            val stackParam = LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                            )

                            stack.layoutParams = stackParam
                            stack.orientation = LinearLayout.HORIZONTAL

                            stack.addView(addCategoryItem(it))

                            contentStack.addView(stack)
                        } else {
                            val lastStack = contentStack.children.last() as LinearLayout
                            lastStack.addView(addCategoryItem(it))
                        }
                        numOfCategories += 1
                    }
                    binding!!.contentLoadingBar.isVisible = false

                    runBlocking {
                        val job = GlobalScope.launch(Dispatchers.Default) {
                            GetLatestContent(categories)
                        }
                    }
                }
            }
        }


        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //--- ratings window ---
        if(!DataManager.getHasShownRatingsPopUp(requireContext())){
            if(DataManager.shouldShowRatingsPopUp(requireContext())){
                showPopupWindow()
            }
        } else {
            if(!DataManager.shouldShowRatingsPopUp(requireContext())){
                DataManager.setHasShownRatingsPopUp(requireContext(), false)
            }
        }

        binding!!.swipeContainer.setOnRefreshListener {

            refreshList()

        }



        // Configure the refreshing colors

        binding!!.swipeContainer.setColorSchemeResources(R.color.darkOgiOgi,

            R.color.darkerOgiOgi,

            R.color.lightOgiOgi,

            R.color.mediumOgiOgi,

            R.color.yellowOgiOgi)

        //binding!!.swipeContainer.setSlingshotDistance(5)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        //_binding = null
    }

    fun addCategoryItem(type: CategoryType): CardView{

        //--- container card ---
        val card = CardView(requireContext())

        val cardParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        cardParams.setMargins(20,20,20,20)

        card.radius = 40f
        card.setCardBackgroundColor(Color.rgb(255,255,255))
        card.layoutParams = cardParams
        card.cardElevation = 10f

        //--- stack? ---
        val stack = LinearLayout(requireContext())

        val stackParam = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )

        stack.layoutParams = stackParam
        stack.orientation = LinearLayout.VERTICAL


        //--- top card ---
        val topCard = CardView(requireContext())

        val topParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            300
        )

        topCard.radius = 0f
        topCard.setCardBackgroundColor(Color.parseColor(type.color))
        topCard.setContentPadding(10,10,10,10)
        topCard.layoutParams = topParams
        topCard.cardElevation = 0f


        val imageView = ImageView(requireContext())

        val imageParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        imageParams.setMargins(0,0,0,0)

        imageView.layoutParams = imageParams
        API.getImage(requireContext(), type.icon_url, imageView)

        topCard.addView(imageView)

        stack.addView(topCard)

        val set = ConstraintSet()
        set.connect(topCard.id, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP)

        //--- bottom card ---
        val bottomCard = CardView(requireContext())

        val bottomParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            100
        )

        bottomCard.radius = 20f
        bottomCard.setCardBackgroundColor(Color.rgb(255,255,255))
        bottomCard.setContentPadding(10,15,10,10)
        bottomCard.layoutParams = bottomParams
        bottomCard.cardElevation = 0f


        val textView = TextView(requireContext())
        val textParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            100
        )
        textParams.setMargins(20, 10, 10, 20)

        textView.layoutParams = textParams
        if(this.language == 0 ){
            textView.text = type.title_cy
        } else if(this.language == 1){
            textView.text = type.title_en
        }

        API.loadFont(requireContext(), "Roboto", "700"){
            textView.setTypeface(it)
            textView.setTextSize(18f)
            textView.setTextColor(Color.parseColor("#0F4936"))
        }

        if(Build.VERSION.SDK_INT >= 26)
        {
            textView.setAutoSizeTextTypeUniformWithConfiguration(1,16,1,TypedValue.COMPLEX_UNIT_DIP)
        }

        bottomCard.addView(textView)

        stack.addView(bottomCard)

        card.addView(stack)

        //--- button ---
        val button = Button(requireContext())

        val buttonParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )

        button.layoutParams = buttonParams
        button.setBackgroundColor(Color.TRANSPARENT)
        button.setOnClickListener({
            val action = HomeFragmentDirections.actionNavigationHomeToCategoryTypeFragment(type)
            findNavController().navigate(action)
        })

        card.addView(button)

        set.connect(button.id, ConstraintSet.TOP, card.id, ConstraintSet.TOP)
        set.connect(button.id, ConstraintSet.BOTTOM, card.id, ConstraintSet.BOTTOM)
        set.connect(button.id, ConstraintSet.LEFT, card.id, ConstraintSet.LEFT)
        set.connect(button.id, ConstraintSet.RIGHT, card.id, ConstraintSet.RIGHT)




        return card
    }

    fun GetLatestContent(c: ArrayList<CategoryType>){
        requireActivity().runOnUiThread { binding!!.latestLoadingBar.isVisible = true }

        val latestMap = mutableMapOf<Int,Date>()
        val latestArray = ArrayList<ContentItem>()
        var i = 0
        c.forEach{ type ->
            API.GetContentItems(requireContext(), type.id) { items ->
                println("content items: " + items.size)
                items.forEach{ child ->
                    if(child.is_folder){
                        child.children.forEach{ subChild ->
                            val formatter = SimpleDateFormat("E d MMM yyyy HH:mm", Locale.ENGLISH)
                            val date = formatter.parse(subChild.updated)
                            latestMap[subChild.id] = date
                        }
                    } else {
                        val formatter = SimpleDateFormat("E d MMM yyyy HH:mm", Locale.ENGLISH)
                        val date = formatter.parse(child.updated)
                        latestMap[child.id] = date
                    }

                    latestArray.add(child)
                }
                i++

                if(i == c.size){
                    latestArray.sort()
                    val shortArray = latestArray.take(5)
                    shortArray.forEach{
                        API.GetContentItemByID(mainContext!!, it.id) { item ->

                            val card = CardView(mainContext!!)

                            val cardParams = LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                200
                            )
                            cardParams.setMargins(20,20,20,20)

                            card.radius = 40f
                            card.setCardBackgroundColor(Color.rgb(255,255,255))
                            card.setContentPadding(10,10,10,10)
                            card.layoutParams = cardParams
                            card.cardElevation = 10f
//--- stack ---
                            val stack = LinearLayout(mainContext!!)

                            val stackParam = LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.MATCH_PARENT
                            )

                            stack.layoutParams = stackParam
                            stack.orientation = LinearLayout.HORIZONTAL

                            //--- left card ---
                            val leftCard = CardView(mainContext!!)

                            val leftParams = LinearLayout.LayoutParams(
                                200,
                                LinearLayout.LayoutParams.MATCH_PARENT
                            )

                            leftCard.radius = 20f
                            leftCard.setCardBackgroundColor(Color.rgb(255,255,255))
                            leftCard.setContentPadding(10,10,10,10)
                            leftCard.layoutParams = leftParams
                            leftCard.elevation = 0f

                            val imageView = ImageView(mainContext!!)

                            val imageParams = LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.MATCH_PARENT
                            )
                            imageParams.setMargins(0,0,0,0)

                            imageView.layoutParams = imageParams
                            API.getImage(mainContext!!, item.icon_url, imageView)

                            val blobView = ImageView(mainContext!!)

                            blobView.layoutParams = imageParams
                            blobView.setImageDrawable(mainContext!!.getDrawable(R.drawable.ic_blob))
                            API.GetCategoryTypeByID(mainContext!!, item.type) {
                                blobView.setColorFilter(Color.parseColor(it.color))
                            }

                            leftCard.addView(blobView)
                            leftCard.addView(imageView)

                            stack.addView(leftCard)

                            //--- right card ---
                            val rightCard = CardView(mainContext!!)

                            val rightParams = LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.MATCH_PARENT
                            )

                            rightCard.radius = 20f
                            rightCard.setCardBackgroundColor(Color.rgb(255,255,255))
                            rightCard.setContentPadding(20,10,10,10)
                            rightCard.layoutParams = rightParams
                            rightCard.elevation = 0f

                            val rightStack = LinearLayout(mainContext!!)
                            val rightStackParam = LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.MATCH_PARENT
                            )

                            rightStack.layoutParams = rightStackParam
                            rightStack.orientation = LinearLayout.VERTICAL

                            val textView = TextView(mainContext!!)

                            if(this.language == 0 ){
                                textView.text = item.title_cy
                            } else if(this.language == 1){
                                textView.text = item.title_en
                            }

                            textView.gravity = Gravity.CENTER

                            if(Build.VERSION.SDK_INT >= 26)
                            {
                                textView.setAutoSizeTextTypeUniformWithConfiguration(1,18,1,TypedValue.COMPLEX_UNIT_DIP)
                            }

                            API.loadFont(mainContext!!, "Roboto", "700"){
                                textView.setTypeface(it)
                                textView.setTextSize(18f)
                                textView.setTextColor(Color.parseColor("#0F4936"))
                            }

                            val categoryText = TextView(mainContext!!)

                            println("getting cat: " + item.type)
                            API.GetCategoryTypeByID(mainContext!!, item.type) {

                                if(this.language == 0 ){
                                    categoryText.text = it.title_cy
                                } else if(this.language == 1){
                                    categoryText.text = it.title_en
                                }
                                categoryText.setTextColor(Color.parseColor(it.color))
                            }
                            categoryText.gravity = Gravity.CENTER_VERTICAL

                            API.loadFont(mainContext!!, "Roboto", "700"){
                                categoryText.setTypeface(it)
                                categoryText.setTextSize(16f)
                            }

                            if(Build.VERSION.SDK_INT >= 26)
                            {
                                categoryText.setAutoSizeTextTypeUniformWithConfiguration(1,16,1,TypedValue.COMPLEX_UNIT_DIP)
                            }

                            rightStack.addView(categoryText)
                            rightStack.addView(textView)
                            rightCard.addView(rightStack)
                            stack.addView(rightCard)

                            card.addView(stack)

                            //--- button ---
                            val button = Button(mainContext!!)

                            val buttonParams = LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.MATCH_PARENT
                            )

                            button.layoutParams = buttonParams
                            button.setBackgroundColor(Color.TRANSPARENT)
                            button.setOnClickListener({
                                API.GetCategoryTypeByID(mainContext!!, item.type) {
                                    type ->
                                    val action = HomeFragmentDirections.actionNavigationHomeToContentItemFragment(item, type)
                                    findNavController().navigate(action)
                                }
                            })

                            card.addView(button)

                            binding!!.latestContentStack.addView(card)
                        }
                    }
                }
            }
        }
        requireActivity().runOnUiThread { binding!!.latestLoadingBar.isVisible = false }
    }

    fun showPopupWindow(){

        DataManager.setHasShownRatingsPopUp(requireContext(), true)

        val popup = InitialPopUp()
        popup.show(childFragmentManager, "initial_window")

//        val popupView = LayoutInflater.from(activity).inflate(R.layout.popup_window, null)
//        val window = PopupWindow(popupView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
//
//        window.showAsDropDown(popupView, 0, 0)
    }

    fun getHeader(){

        val retrievedFam = DataManager.getLatestFamily(requireContext())

        var lang = ""

        if(this.language == 0){
            lang = "cy"
        } else if(this.language == 1){
            lang = "en"
        }

        val localeContext = LocaleHelper.setLocale(requireActivity(), lang)
        val localeResources = localeContext.resources

        val testView: TextView = binding!!.familyHeader


        var date = Date()
        val cal = Calendar.getInstance()
        cal.time = date
        var hour = cal.get(Calendar.HOUR_OF_DAY)
        if(hour > 11 && hour <= 17){
            if(DataManager.getHasSkippedFamily(mainContext!!)){
                testView.text = ""+ localeResources!!.getString(R.string.home_evening) + "!"
            } else {
                testView.text = ""+ localeResources!!.getString(R.string.home_evening) + " " + retrievedFam!!.parents[0].first_name + "!"
            }
            testView.setTextColor(Color.WHITE)
            binding!!.imageView.setImageDrawable(requireActivity().getDrawable(R.drawable.ic_timing_prynhawn_da))
        } else if(hour > 17){
            if(DataManager.getHasSkippedFamily(mainContext!!)){
                testView.text = ""+ localeResources!!.getString(R.string.home_night) + "!"
            } else {
                testView.text = ""+ localeResources!!.getString(R.string.home_night) + " " + retrievedFam!!.parents[0].first_name + "!"
            }
            testView.setTextColor(Color.WHITE)
            binding!!.imageView.setImageDrawable(requireActivity().getDrawable(R.drawable.ic_timing_noswaith_dda))
        } else if(hour <= 11){
            if(DataManager.getHasSkippedFamily(mainContext!!)){
                testView.text = ""+ localeResources!!.getString(R.string.home_good_morning) + "!"
            } else {
                testView.text = ""+ localeResources!!.getString(R.string.home_good_morning) + " " + retrievedFam!!.parents[0].first_name + "!"
            }
            testView.setTextColor(Color.WHITE)
            binding!!.imageView.setImageDrawable(requireActivity().getDrawable(R.drawable.ic_timing_bore_da))
        }
    }


    fun refreshList(){


        binding!!.contentStack.removeAllViews()
        binding!!.latestContentStack.removeAllViews()

        val contentStack: LinearLayout = binding!!.contentStack


        runBlocking {
            val job = GlobalScope.launch(Dispatchers.Default) {
                binding!!.contentLoadingBar.isVisible = true
                var numOfCategories = 0
                API.GetCategories(requireContext()) {
                    categories = it
                    it.forEach {
                        if(numOfCategories % 2 == 0){
                            val stack = LinearLayout(requireContext())

                            val stackParam = LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                            )

                            stack.layoutParams = stackParam
                            stack.orientation = LinearLayout.HORIZONTAL

                            stack.addView(addCategoryItem(it))

                            contentStack.addView(stack)
                        } else {
                            val lastStack = contentStack.children.last() as LinearLayout
                            lastStack.addView(addCategoryItem(it))
                        }
                        numOfCategories += 1
                    }
                    binding!!.contentLoadingBar.isVisible = false

                    runBlocking {
                        val job = GlobalScope.launch(Dispatchers.Default) {
                            GetLatestContent(categories)
                            binding!!.swipeContainer.isRefreshing = false
                        }
                    }
                }
            }
        }
    }


}
