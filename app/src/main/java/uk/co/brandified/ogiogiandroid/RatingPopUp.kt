package uk.co.brandified.ogiogiandroid

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import uk.co.brandified.ogiogiandroid.api.API
import uk.co.brandified.ogiogiandroid.api.DataManager
import uk.co.brandified.ogiogiandroid.api.FamilySendJson
import uk.co.brandified.ogiogiandroid.api.Parent
import uk.co.brandified.ogiogiandroid.databinding.RatingWindowBinding
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class RatingPopUp: DialogFragment() {

    private var _binding: RatingWindowBinding? = null

    private val binding get() = _binding!!

    private var confidenceStars: ArrayList<ImageView> = ArrayList()
    private var importanceStars: ArrayList<ImageView> = ArrayList()
    private var shareStars: ArrayList<ImageView> = ArrayList()

    private var confidenceCount: Int = 0
    private var importanceCount: Int = 0
    private var shareCount: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return super.onCreateView(inflater, container, savedInstanceState)

        _binding = RatingWindowBinding.inflate(inflater, container, false)
        val root: View = binding!!.root

        var view = inflater.inflate(R.layout.initial_window, null)


        API.loadFont(requireContext(), "Roboto", "700") {
            binding.popupTitle.setTypeface(it)
        }

        API.loadFont(requireContext(), "Roboto", "400") {
            binding.popupSubTitle.setTypeface(it)
        }

        for (i in 0..binding.confidenceStarsStack.childCount - 1) {
            var star = binding.confidenceStarsStack.getChildAt(i) as ImageView
            star.setColorFilter(Color.parseColor("#F0BC41"))
            star.alpha = 0.4f
            star.setOnClickListener {
                confidenceStarClicked(star)
            }
            confidenceStars.add(star)
        }

        for (i in 0..binding.importanceStarsStack.childCount - 1) {
            var star = binding.importanceStarsStack.getChildAt(i) as ImageView
            star.setColorFilter(Color.parseColor("#F0BC41"))
            star.alpha = 0.4f
            star.setOnClickListener {
                importanceStarClicked(star)
            }
            importanceStars.add(star)
        }

        for (i in 0..binding.shareStarsStack.childCount - 1) {
            var star = binding.shareStarsStack.getChildAt(i) as ImageView
            star.setColorFilter(Color.parseColor("#F0BC41"))
            star.alpha = 0.4f
            star.setOnClickListener {
                shareStarClicked(star)
            }
            shareStars.add(star)
        }


        val latestFamily = DataManager.getLatestFamily(requireContext())
        val ratings = latestFamily!!.family_ratings

        confidenceCount = ratings!!.parent_welsh_confidence
        importanceCount = ratings!!.parent_welsh_importance
        shareCount = ratings!!.parent_share_welsh

        setupStars()



        binding.popupButton.setOnClickListener {


            ratings.parent_welsh_confidence = confidenceCount
            ratings.parent_welsh_importance = importanceCount
            ratings.parent_share_welsh = shareCount

            latestFamily.family_ratings = ratings

            val parents = latestFamily.parents

            var mainParent = Parent()
            parents.forEach{
                par ->

                if(par.main_phone){
                    mainParent = par
                }
            }

            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ")
            val current = sdf.format(Date())

            latestFamily.send_date = current

            DataManager.saveLatestFamily(latestFamily, requireContext())

            val format = SimpleDateFormat("yyyy-MM-dd")
            val date = format.format(Date())

            var firstLang = ""
            var secondLang = ""

//            if(mainParent.first_language == 1){
//                firstLang = "cy"
//            } else {
//                firstLang = "en"
//            }
//
//            if(mainParent.second_language == 1) {
//                secondLang = "cy"
//            } else {
//                secondLang = "en"
//            }


            val fam = FamilySendJson(
                latestFamily.family_id,
                date,
                latestFamily.items_read,
                latestFamily.post_code,
                mainParent.first_language,
                mainParent.second_language,
                latestFamily!!.family_ratings!!.parent_share_welsh,
                latestFamily!!.family_ratings!!.parent_welsh_confidence,
                latestFamily!!.family_ratings!!.parent_welsh_importance,
                latestFamily.children.size
            )

            API.SendFamilyJSON(fam, requireContext()){

                dismiss()
            }
        }


        return root
    }

    fun confidenceStarClicked(star: ImageView) {
        var i = 0
        confidenceStars.forEach {
            if (it == star) {

                confidenceCount = i + 1
                for (j in 0..4) {
                    if (j <= i) {
                        var star = binding.confidenceStarsStack.getChildAt(j) as ImageView
                        star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star))
                        star.alpha = 1f
                    } else {
                        var star = binding.confidenceStarsStack.getChildAt(j) as ImageView
                        star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star_outline))
                        star.alpha = 0.4f
                    }
                }
            }
            i++
        }
    }

    fun importanceStarClicked(star: ImageView) {
        var i = 0
        importanceStars.forEach {
            if (it == star) {

                importanceCount = i + 1

                for (j in 0..4) {
                    if (j <= i) {
                        var star = binding.importanceStarsStack.getChildAt(j) as ImageView
                        star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star))
                        star.alpha = 1f
                    } else {
                        var star = binding.importanceStarsStack.getChildAt(j) as ImageView
                        star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star_outline))
                        star.alpha = 0.4f
                    }
                }
            }
            i++
        }
    }

    fun shareStarClicked(star: ImageView) {
        var i = 0
        shareStars.forEach {
            if (it == star) {

                shareCount = i + 1

                for (j in 0..4) {
                    if (j <= i) {
                        var star = binding.shareStarsStack.getChildAt(j) as ImageView
                        star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star))
                        star.alpha = 1f
                    } else {
                        var star = binding.shareStarsStack.getChildAt(j) as ImageView
                        star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star_outline))
                        star.alpha = 0.4f
                    }
                }
            }
            i++
        }
    }

    fun setupStars() {
        if (this.confidenceCount > 0) {
            var i = 0
            for (i in 0..confidenceCount - 1) {
                var star = binding.confidenceStarsStack.getChildAt(i) as ImageView
                star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star))
                star.alpha = 1f
            }
        }

        if (this.importanceCount > 0) {
            var i = 0
            for (i in 0..importanceCount - 1) {
                var star = binding.importanceStarsStack.getChildAt(i) as ImageView
                star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star))
                star.alpha = 1f
            }
        }

        if (this.shareCount > 0) {
            var i = 0
            for (i in 0..shareCount - 1) {
                var star = binding.shareStarsStack.getChildAt(i) as ImageView
                star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star))
                star.alpha = 1f
            }
        }

    }
}