package uk.co.brandified.ogiogiandroid

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import uk.co.brandified.ogiogiandroid.R
import uk.co.brandified.ogiogiandroid.api.API
import uk.co.brandified.ogiogiandroid.api.CategoryType
import uk.co.brandified.ogiogiandroid.api.ContentItem
import uk.co.brandified.ogiogiandroid.api.DataManager
import uk.co.brandified.ogiogiandroid.databinding.FragmentCategoryTypeBinding
import uk.co.brandified.ogiogiandroid.databinding.FragmentHomeBinding
import uk.co.brandified.ogiogiandroid.ui.home.HomeFragmentDirections
import uk.co.brandified.ogiogiandroid.ui.home.HomeViewModel

class CategoryTypeFragment : Fragment(R.layout.fragment_category_type) {
    private var _binding: FragmentCategoryTypeBinding? = null


    private var type: CategoryType? = null

    private val args: CategoryTypeFragmentArgs by navArgs()

    private val binding get() = _binding!!

    private var mainContext: Context? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentCategoryTypeBinding.inflate(inflater, container, false)
        val root: View = binding!!.root

        mainContext = requireContext()
        // Inflate the layout for this fragment

        this.type = args.type

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(args.isNested){

            val item = args.nestedItem

            val language = DataManager.getLocalLanguage(mainContext!!)

            if(language == 0){
                binding.typeTitle.text = item!!.title_cy
                binding.typeSubtitle.text = type!!.description_cy
            } else if(language == 1){
                binding.typeTitle.text = item!!.title_en
                binding.typeSubtitle.text = type!!.description_en
            }

            binding.typeTitle.setTextColor(Color.parseColor(type!!.color))
            binding.typeSubtitle.setTextColor(Color.WHITE)

            API.getImage(requireContext(), type!!.icon_url, binding.typeImage)
            getNestedList(item!!)
        } else {
            val language = DataManager.getLocalLanguage(mainContext!!)

            if(language == 0){
                binding.typeTitle.text = type!!.title_cy
                binding.typeSubtitle.text = type!!.description_cy
            } else if(language == 1){
                binding.typeTitle.text = type!!.title_en
                binding.typeSubtitle.text = type!!.description_en
            }

            binding.typeTitle.setTextColor(Color.parseColor(type!!.color))
            binding.typeSubtitle.setTextColor(Color.WHITE)

            API.getImage(mainContext!!, type!!.icon_url, binding.typeImage)
            getList()
        }

    }

    fun getList(){

        binding!!.loadingBar.isVisible = true
        API.GetContentItems(mainContext!!, type!!.id){
            items ->

            items.forEach{
                item ->

                val card = CardView(mainContext!!)

                val cardParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    200
                )
                cardParams.setMargins(20,20,20,20)

                card.radius = 40f
                card.setCardBackgroundColor(Color.rgb(255,255,255))
                card.setContentPadding(10,10,10,10)
                card.layoutParams = cardParams
                card.cardElevation = 10f

                //--- stack ---
                val stack = LinearLayout(mainContext!!)

                val stackParam = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
                )

                stack.layoutParams = stackParam
                stack.orientation = LinearLayout.HORIZONTAL

                //--- left card ---
                val leftCard = CardView(mainContext!!)

                val leftParams = LinearLayout.LayoutParams(
                    200,
                    LinearLayout.LayoutParams.MATCH_PARENT
                )

                leftCard.radius = 20f

                leftCard.setCardBackgroundColor(Color.rgb(255,255,255))
                leftCard.setContentPadding(10,10,10,10)
                leftCard.layoutParams = leftParams
                leftCard.elevation = 0f

                val imageView = ImageView(mainContext!!)

                val imageParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
                )
                imageParams.setMargins(0,0,0,0)

                imageView.layoutParams = imageParams
                API.getImage(mainContext!!, item.icon_url, imageView)

                val blobView = ImageView(mainContext!!)

                blobView.layoutParams = imageParams
                blobView.setImageDrawable(mainContext!!.getDrawable(R.drawable.ic_blob))
                blobView.setColorFilter(Color.parseColor(type!!.color))

                leftCard.addView(blobView)

                leftCard.addView(imageView)

                stack.addView(leftCard)

                //--- right card ---
                val rightCard = CardView(mainContext!!)

                val rightParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
                )

                rightCard.radius = 20f
                rightCard.setCardBackgroundColor(Color.rgb(255,255,255))
                rightCard.setContentPadding(20,10,10,10)
                rightCard.layoutParams = rightParams
                rightCard.elevation = 0f

                val textView = TextView(mainContext!!)

                val language = DataManager.getLocalLanguage(mainContext!!)

                if(language == 0){
                    textView.text = item.title_cy
                } else if(language == 1){
                    textView.text = item.title_en
                }

                textView.gravity = Gravity.CENTER
                textView.textAlignment = View.TEXT_ALIGNMENT_VIEW_START

                API.loadFont(mainContext!!, "Roboto", "700"){
                    textView.setTypeface(it)
                    textView.setTextSize(18f)
                    textView.setTextColor(Color.parseColor("#0F4936"))
                }

                rightCard.addView(textView)
                stack.addView(rightCard)

                card.addView(stack)

                //--- button ---
                val button = Button(mainContext!!)

                val buttonParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
                )

                button.layoutParams = buttonParams
                button.setBackgroundColor(Color.TRANSPARENT)
                button.setOnClickListener({

                    if(item.is_folder) {
                        val action = CategoryTypeFragmentDirections.actionCategoryTypeFragmentToCategoryTypeFragment(type!!)
                        action.isNested = true
                        action.nestedItem = item
                        findNavController().navigate(action)
                    } else {
                        val action = CategoryTypeFragmentDirections.actionCategoryTypeFragmentToContentItemFragment(item, type!!)
                        findNavController().navigate(action)
                    }
                })

                card.addView(button)

                val set = ConstraintSet()
                set.connect(button.id, ConstraintSet.TOP, card.id, ConstraintSet.TOP)
                set.connect(button.id, ConstraintSet.BOTTOM, card.id, ConstraintSet.BOTTOM)
                set.connect(button.id, ConstraintSet.LEFT, card.id, ConstraintSet.LEFT)
                set.connect(button.id, ConstraintSet.RIGHT, card.id, ConstraintSet.RIGHT)

                binding!!.itemsStack.addView(card)
            }

            binding!!.loadingBar.isVisible = false
        }
    }

    fun getNestedList(item: ContentItem){
        binding!!.loadingBar.isVisible = true
        item.children.forEach{
            child ->

            val card = CardView(mainContext!!)

            val cardParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                200
            )
            cardParams.setMargins(20,20,20,20)

            card.radius = 40f
            card.setCardBackgroundColor(Color.WHITE)
            card.setContentPadding(10,10,10,10)
            card.layoutParams = cardParams
            card.cardElevation = 10f

            //--- stack ---
            val stack = LinearLayout(mainContext!!)

            val stackParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            stack.layoutParams = stackParam
            stack.orientation = LinearLayout.HORIZONTAL

            //--- left card ---
            val leftCard = CardView(mainContext!!)

            val leftParams = LinearLayout.LayoutParams(
                200,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            leftCard.radius = 20f
            leftCard.setCardBackgroundColor(Color.rgb(255,255,255))
            leftCard.setContentPadding(10,10,10,10)
            leftCard.layoutParams = leftParams
            leftCard.elevation = 0f

            val imageView = ImageView(mainContext!!)

            val imageParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            imageParams.setMargins(0,0,0,0)

            imageView.layoutParams = imageParams
            API.getImage(mainContext!!, child.icon_url, imageView)

            val blobView = ImageView(mainContext!!)

            blobView.layoutParams = imageParams
            blobView.setImageDrawable(resources.getDrawable(R.drawable.ic_blob))
            blobView.setColorFilter(Color.parseColor(type!!.color))

            leftCard.addView(blobView)

            leftCard.addView(imageView)

            stack.addView(leftCard)

            //--- right card ---
            val rightCard = CardView(mainContext!!)

            val rightParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            rightCard.radius = 20f
            rightCard.setCardBackgroundColor(Color.rgb(255,255,255))
            rightCard.setContentPadding(20,10,10,10)
            rightCard.layoutParams = rightParams
            rightCard.elevation = 0f

            val textView = TextView(mainContext!!)
            textView.text = child.title_cy
            textView.gravity = Gravity.CENTER

            API.loadFont(mainContext!!, "Roboto", "700"){
                textView.setTypeface(it)
                textView.setTextSize(18f)
                textView.setTextColor(Color.parseColor("#0F4936"))
            }

            rightCard.addView(textView)
            stack.addView(rightCard)

            card.addView(stack)

            //--- button ---
            val button = Button(mainContext!!)

            val buttonParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            button.layoutParams = buttonParams
            button.setBackgroundColor(Color.TRANSPARENT)
            button.setOnClickListener({

                if(child.is_folder) {
                    val action = CategoryTypeFragmentDirections.actionCategoryTypeFragmentToCategoryTypeFragment(type!!)
                    action.isNested = true
                    action.nestedItem = child
                    findNavController().navigate(action)
                } else {
                    val action = CategoryTypeFragmentDirections.actionCategoryTypeFragmentToContentItemFragment(child, type!!)
                    findNavController().navigate(action)
                }
            })

            card.addView(button)

            val set = ConstraintSet()
            set.connect(button.id, ConstraintSet.TOP, card.id, ConstraintSet.TOP)
            set.connect(button.id, ConstraintSet.BOTTOM, card.id, ConstraintSet.BOTTOM)
            set.connect(button.id, ConstraintSet.LEFT, card.id, ConstraintSet.LEFT)
            set.connect(button.id, ConstraintSet.RIGHT, card.id, ConstraintSet.RIGHT)

            binding!!.itemsStack.addView(card)
        }
        binding!!.loadingBar.isVisible = false
    }
}