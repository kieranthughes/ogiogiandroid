package uk.co.brandified.ogiogiandroid

import android.content.Context.INPUT_METHOD_SERVICE
import android.graphics.Color
import android.opengl.Visibility
import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import uk.co.brandified.ogiogiandroid.api.DataManager
import uk.co.brandified.ogiogiandroid.api.Languages
import uk.co.brandified.ogiogiandroid.api.Parent
import uk.co.brandified.ogiogiandroid.api.Ratings
import uk.co.brandified.ogiogiandroid.databinding.FragmentAddParentBinding
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.*
import java.util.*
import java.util.Calendar.*


class AddParentFragment : Fragment(),  AdapterView.OnItemSelectedListener{
    private var _binding: FragmentAddParentBinding? = null

    private val binding get() = _binding!!

    private var confidenceStars: ArrayList<ImageView> = ArrayList()
    private var importanceStars: ArrayList<ImageView> = ArrayList()
    private var shareStars: ArrayList<ImageView> = ArrayList()

    private var confidenceCount: Int = 0
    private var importanceCount: Int = 0
    private var shareCount: Int = 0

    private var firstLanguage: String = ""
    private var secondLanguage: String = ""

    private var language = 0

    private var isMainParent = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentAddParentBinding.inflate(inflater, container, false)
        val root: View = binding!!.root


        this.language = DataManager.getLocalLanguage(requireContext())


        var lang = ""

        if(this.language == 0){
            lang = "cy"
        } else if(this.language == 1){
            lang = "en"
        }

        val localeContext = LocaleHelper.setLocale(requireActivity(), lang)
        val localeResources = localeContext.resources

        binding.parentTitle.text = localeResources.getString(R.string.add_parent_title)


        var childText = localeResources.getString(R.string.add_parent_subtitle)
        var content = SpannableString(localeResources.getString(R.string.add_parent_subtitle))

        fun ignoreCaseOpt(ignoreCase: Boolean) =
            if (ignoreCase) setOf(RegexOption.IGNORE_CASE) else emptySet()

        fun String?.indexesOf(pat: String, ignoreCase: Boolean = true): List<Int> =
            pat.toRegex(ignoreCaseOpt(ignoreCase))
                .findAll(this?: "")
                .map { it.range.first }
                .toList()

        var sIndex = 0
        if(this.language == 0){
            sIndex = childText.indexesOf("Darllen mwy...", true).first()
        } else if(this.language == 1){
            sIndex = childText.indexesOf("Read more...", true).first()
        }

        content.setSpan(UnderlineSpan(), sIndex, content.length, 0)
        binding.childTitle.text = content

        binding.childTitle.setOnClickListener {
            val action = AddParentFragmentDirections.actionAddParentFragmentToDataFragment()
            findNavController().navigate(action)
        }



        binding.firstNameTitle.text = localeResources.getString(R.string.add_first_name)
        binding.AgeTitle.text = localeResources.getString(R.string.add_age)
        binding.FirstLanguageTitle.text = localeResources.getString(R.string.add_first_language)
        binding.SecondLanguageTitle.text = localeResources.getString(R.string.add_second_language)
        binding.question1.text = localeResources.getString(R.string.progress_question_1)
        binding.question2.text = localeResources.getString(R.string.progress_question_2)
        binding.question3.text = localeResources.getString(R.string.progress_question_3)
        binding.postCode.text = localeResources.getString(R.string.add_postcode)
        binding.finishButton.text = localeResources.getString(R.string.family_save)
        binding.ageWrongFormat.text = localeResources.getString(R.string.parent_age_error)








        binding.finishButton.setOnClickListener {

            var age = 0

            if (Build.VERSION.SDK_INT > 26) {
                //--- this seems dodgy ---
                val sdf = SimpleDateFormat("yyyy-MM-dd")

                try {
                    val d = sdf.parse(binding.ageBox.text.toString())
                    val currentDate = Date()

                    val a = Calendar.getInstance(Locale.ENGLISH)
                    a.time = d

                    val b = Calendar.getInstance(Locale.ENGLISH)
                    b.time = currentDate

                    var diff = b.get(YEAR) - a.get(YEAR)
                    if (a.get(MONTH) > b.get(MONTH) ||
                        (a.get(MONTH) == b.get(MONTH) && a.get(DATE) > b.get(DATE))) {
                        diff--;
                    }

                    age = diff

                    binding.ageWrongFormat.visibility = View.INVISIBLE;

                    val fam = DataManager.getLatestFamily(requireContext())


                    var main = false
                    if(fam!!.parents.isEmpty()){
                        main = true
                    }
                    fam!!.parents.forEach{
                        if(it.main_phone){
                            main = false
                        } else {
                            main = true
                        }
                    }


                    var p = Parent(
                        first_name = binding.firstNameBox.text.toString(),
                        age,
                        firstLanguage,
                        secondLanguage,
                        main,
                        1,
                        0,
                        binding.ageBox.text.toString()
                    )
                    fam?.parents?.add(p)

                    if(main){
                        fam?.post_code = binding.postCodeBox.text.toString()

                        val ratings = Ratings(
                            confidenceCount,
                            importanceCount,
                            shareCount
                        )

                        fam?.family_ratings = ratings
                    }


                    DataManager.saveLatestFamily(fam!!, requireContext())

                    println("saved: " + DataManager.getLatestFamily(requireContext())?.parents?.size)

                    requireActivity().onBackPressed()




                } catch(pe: ParseException){
                    binding.ageWrongFormat.visibility = View.VISIBLE
                    binding.mainScroll.scrollTo(0,binding.ageBox.top + 20)
                }
            }

        }


        val spinnerArray = ArrayList<String>()
        val sorted = Languages.map.toSortedMap()


        sorted.forEach{
                (key, value) ->

            spinnerArray.add(key)
        }

        var adapter = ArrayAdapter<String>(
            requireContext(), android.R.layout.simple_spinner_item, spinnerArray
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.firstLanguageSpinner.adapter = adapter
        binding.secondLanguageSpinner.adapter = adapter


//        ArrayAdapter.createFromResource(
//            requireContext(), R.array.language_array,
//            android.R.layout.simple_spinner_item
//        ).also {
//                adapter ->
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//
//            binding.firstLanguageSpinner.adapter = adapter
//            binding.secondLanguageSpinner.adapter = adapter
//        }

        binding.firstLanguageSpinner.onItemSelectedListener = this
        binding.secondLanguageSpinner.onItemSelectedListener = this


        var fam = DataManager.getLatestFamily(requireContext())
        var main = false
        if(fam!!.parents.isEmpty()){
            main = true
        } else {
            main = false
        }

        fam!!.parents.forEach{
            if(it.main_phone){
                main = false
            } else {
                main = true
            }
        }

        if(main){
            binding.starStack.visibility = View.VISIBLE
            binding.postCodeBox.visibility = View.VISIBLE
        } else {
            binding.starStack.visibility = View.GONE
            binding.postCodeBox.visibility = View.GONE
        }


        // Inflate the layout for this fragment

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        confidenceStars = ArrayList()
        importanceStars = ArrayList()
        shareStars = ArrayList()

        for (i in 0..binding.confidenceStarsStack.childCount - 1){
            var star = binding.confidenceStarsStack.getChildAt(i) as ImageView
            star.setColorFilter(Color.parseColor("#0F4936"))
            star.alpha = 0.4f
            star.setOnClickListener {
                confidenceStarClicked(star)
            }
            confidenceStars.add(star)
        }

        for (i in 0..binding.importanceStarsStack.childCount - 1){
            var star = binding.importanceStarsStack.getChildAt(i) as ImageView
            star.setColorFilter(Color.parseColor("#0F4936"))
            star.alpha = 0.4f
            star.setOnClickListener {
                importanceStarClicked(star)
            }
            importanceStars.add(star)
        }

        for (i in 0..binding.shareStarsStack.childCount - 1){
            var star = binding.shareStarsStack.getChildAt(i) as ImageView
            star.setColorFilter(Color.parseColor("#0F4936"))
            star.alpha = 0.4f
            star.setOnClickListener {
                shareStarClicked(star)
            }
            shareStars.add(star)
        }

    }

    fun confidenceStarClicked(star: ImageView) {
        var i = 0
        confidenceStars.forEach{
            if(it == star) {

                confidenceCount = i + 1
                for(j in 0..4){
                    if(j <= i){
                        var star = binding.confidenceStarsStack.getChildAt(j) as ImageView
                        star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star))
                        star.alpha = 1f
                    } else {
                        var star = binding.confidenceStarsStack.getChildAt(j) as ImageView
                        star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star_outline))
                        star.alpha = 0.4f
                    }
                }
            }
            i++
        }
    }

    fun importanceStarClicked(star: ImageView) {
        var i = 0
        importanceStars.forEach{
            if(it == star) {

                importanceCount = i + 1

                for(j in 0..4){
                    if(j <= i){
                        var star = binding.importanceStarsStack.getChildAt(j) as ImageView
                        star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star))
                        star.alpha = 1f
                    } else {
                        var star = binding.importanceStarsStack.getChildAt(j) as ImageView
                        star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star_outline))
                        star.alpha = 0.4f
                    }
                }
            }
            i++
        }
    }

    fun shareStarClicked(star: ImageView) {
        var i = 0
        shareStars.forEach{
            if(it == star) {

                shareCount = i + 1

                for(j in 0..4){
                    if(j <= i){
                        var star = binding.shareStarsStack.getChildAt(j) as ImageView
                        star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star))
                        star.alpha = 1f
                    } else {
                        var star = binding.shareStarsStack.getChildAt(j) as ImageView
                        star.setImageDrawable(resources.getDrawable(R.drawable.ic_ios_star_outline))
                        star.alpha = 0.4f
                    }
                }
            }
            i++
        }
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        // An item was selected. You can retrieve the selected item using


        var map = Languages.map.toSortedMap()

        if(parent.id == binding.firstLanguageSpinner.id){
            var i = 0
            map.forEach{
                    (key, value) ->

                if(i == pos){
                    firstLanguage = value
                }

                i++
            }
        }
        if(parent.id == binding.secondLanguageSpinner.id){
            var i = 0
            map.forEach{
                    (key, value) ->

                if(i == pos){
                    secondLanguage = value
                }

                i++
            }
        }

    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Another interface callback
    }

}