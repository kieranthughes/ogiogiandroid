package uk.co.brandified.ogiogiandroid

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import uk.co.brandified.ogiogiandroid.api.*
import uk.co.brandified.ogiogiandroid.databinding.DeleteDataWindowBinding

class DeleteDataPopUp: DialogFragment() {
    private var _binding: DeleteDataWindowBinding? = null

    private val binding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return super.onCreateView(inflater, container, savedInstanceState)

        _binding = DeleteDataWindowBinding.inflate(inflater, container, false)
        val root: View = binding!!.root

        var view = inflater.inflate(R.layout.delete_data_window, null)


//        binding!!.deleteTitle.text = "Anhygoel Sara!"
//        binding!!.deleteSubTitle.text = "'Da chi'n gwneud yn ffantastig yn mynd drwy cynnwys yr app!"


        val language = DataManager.getLocalLanguage(requireContext())

        var lang = ""

        if(language == 0){
            lang = "cy"
        } else if(language == 1){
            lang = "en"
        }

        val localeContext = LocaleHelper.setLocale(requireActivity(), lang)
        val localeResources = localeContext.resources

        binding!!.deleteTitle.text = localeResources.getString(R.string.delete_title)
        binding!!.deleteSubTitle.text = localeResources.getString(R.string.delete_text_1)
        binding!!.deleteSubSubTitle.text = localeResources.getString(R.string.delete_text_2)
        binding!!.deleteWhyTitle.text = localeResources.getString(R.string.delete_why_title)
        binding!!.deleteWhy.text = localeResources.getString(R.string.delete_why)
        binding!!.deleteButton.text = localeResources.getString(R.string.delete_delete)
        binding!!.cancelButton.text = localeResources.getString(R.string.delete_cancel)






        API.loadFont(requireContext(), "Roboto", "700"){
            binding!!.deleteTitle.setTypeface(it)
            binding!!.deleteWhyTitle.setTypeface(it)
        }

        API.loadFont(requireContext(), "Roboto", "500"){
            binding!!.deleteSubTitle.setTypeface(it)
            binding!!.deleteSubSubTitle.setTypeface(it)
            binding!!.deleteWhy.setTypeface(it)
        }

        binding!!.cancelButton.setOnClickListener {
            dismiss()
        }

        binding!!.deleteButton.setOnClickListener {

            val fam = Family(
                family_id = "",
                parents = ArrayList<Parent>(),
                children = ArrayList<Child>(),
                0,
                0,
                0,
                "",
                Ratings(0,0,0),
                ArrayList<Int>(),
                ""
            )

            DataManager.saveInitialFamily(fam, requireContext())
            DataManager.saveLatestFamily(fam, requireContext())
            DataManager.clearViewedItems(requireContext())

            DataManager.setHasSkippedFamily(requireContext(), true)

            dismiss()

//            val ft: FragmentTransaction = parentFragmentManager.beginTransaction()
//            val fragmentToRemove: Fragment? =
//                parentFragmentManager.findFragmentByTag("initial_window")
//            if (fragmentToRemove != null) {
//                ft.remove(fragmentToRemove)
//            }
//            ft.addToBackStack(null)
//            ft.commit() // or ft.commitAllowingStateLoss()
//
//
//            val popup = RatingPopUp()
//            popup.show(parentFragmentManager, "rating_window")

        }

        return root
    }
}